
        var current_row;
        var current_item_id;
        function get_controller(s){
            var re = new RegExp('\\://.*\\..*?/(.*?)/.*','i');
            var res = re.exec(s);
            if(res == null){
        	re = new RegExp('\\://.*\\..*?/(.*)','i');
        	res = re.exec(s);
            }
            if(res && res.length > 1)
                return res[1];
            else
                return '';
        }
        function get_form_data(form_element)
        {
            var data = {};
            form_element.find('.formfield').each(function(index,elem){
                var f = $(elem);
                data[f.attr('name')] = f.val();
                if(f.prop('tagName').toLowerCase() == 'select'){
            	    if(!data['selects']){
            		data['selects'] = [];
            	    }
                    data['selects'][f.attr('name')] = f.children('option:selected').text();
                }
            });
            return data;
        }
        $(function(){
            var controller = '/' + get_controller(window.location.href);
            $('#editdlg').dialog({
                autoOpen:false,
                modal:true,
                buttons:{
                    'Save':function(){
                        var form_data = get_form_data($(this));
                        var dlg = $(this);
                        $.ajax(controller + '/save/'+current_item_id,{
                            data:form_data,
                            dataType:'json',
                            success:function(response){
                                if( response && response.status == 'OK'){
                                    alert('Saved successfully');
                                    var new_row = row_template;
				    form_data['id'] = response.id;
                                    for(var ind in form_data){
                                        new_row = new_row.replace(new RegExp('_'+ind+'_','g'),form_data[ind]);
                                    }
                                    if(form_data['selects']){
                                        for(var ind in form_data['selects']){
                                            new_row = new_row.replace('_'+ind+'+val_',form_data['selects'][ind]);
                                        }
                                    }
                                    new_row += '<td class="op"><a class="editlink" href="'+response.id+'">edit</a></td>';
                                    new_row += '<td class="op"><a class="deletelink" href="'+response.id+'">delete</a></td>';
                                    if(current_row){
                                        current_row.html(new_row);
                                    }
                                    else{
                                	    $('table.admin_table').append('<tr>'+new_row+'</tr>');
                                    }
                                    dlg.dialog('close');
                                }
                                else{
                                    alert('Error saving: ' +( response != null ? response.error : 'empty answer'));
                                }
                            },
                            error:function(xhr,errText,thrown){
                                alert('Error happened: ' + errText)
                            }
                        });
                    },
                    'Cancel':function(){
                        $(this).dialog('close');
                    }
                }
            });
            $('table.admin_table').on('click','tr td a.editlink',function(e){
                e.preventDefault();
                current_row = $(this).parents('tr');
                current_item_id = $(this).attr('href');
                $('#editdlg .formfield').each(function(ind,el){
            	    var $el = $(el);
            	    var fieldname = $el.attr('name');
            	    current_row.find('td').each(function(td_ind, td_el){
            		var $td_el = $(td_el);
            		if($td_el.attr('fieldname') == fieldname){
            		    var tagname = $el.prop('tagName').toLowerCase()
            		    if(tagname == 'input'){
            			$el.val($td_el.text());
            		    }
            		    else if(tagname == 'select'){
            			$el.val($td_el.attr('fieldval'));
            		    }
            		    return false;
            		}
            	    })
                });
                $('#editdlg').dialog("open");
            });
            $('table.admin_table').on('click','tr td a.deletelink',function(e){
                e.preventDefault();
                var link = $(this);
                $.ajax(controller + '/delete/'+link.attr('href'),{
                    dataType:'json',
                    success:function(response){
                        if(response.status == 'OK'){
                            alert('Deleted successfully');
                            link.parents('tr').remove();
                        }
                        else{
                            alert('Error deleting, try once more');
                        }
                    },
                    error:function(xhr,errText,thrown){
                        alert('Error happened: ' + errText)
                    }
                });
            });
            $('#add-item').click(function(e){
                e.preventDefault();
                current_item_id = '';
                current_row = null;
                $('#editdlg').find('.formfield').each(function(ind,el){var $el = $(el); if($el.attr('type') && ($el.attr('type').toLowerCase() != 'hidden')) $el.val('')});
                $('#editdlg').dialog("open");
            });
        });
