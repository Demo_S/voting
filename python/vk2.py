#!/usr/bin/env python

import sys
import re
import requests
import urllib
import json
import time
import MySQLdb as mdb

def get_iph(s):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0 FirePHP/0.7.2'}
    req = s.get('http://vk.com',headers=headers)
    found = re.findall(r'name="ip_h" value="([a-z 0-9]*)"',req.text)
    
    if(len(found)):
		return found[0]
    else:
		return False

def get_login_cookies(s):
    ip_h = get_iph(s)
    if(ip_h):
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0 FirePHP/0.7.2'}
	values = {'act':'login','role':'al_frame','expire':'1','captcha_sid':'','captcha_key':'','_origin':'https://vk.com','ip_h':ip_h,'email':'demo_s@ua.fm','pass':'Cdbltntkm#21'}
	req = s.post('https://login.vk.com/?act=login',data=values,headers=headers)
	found = re.findall(r'remixsid.*?=([a-z 0-9]*?);',req.headers['Set-Cookie'])

	if(len(found)):
	    return found[0]
	else:
	    return False
    else:
	    return False

def authentificate(s):
	remixsid = get_login_cookies(s)
	print remixsid
	if(remixsid):
		req = s.get('https://vk.com')
		page=  req.content
		found = re.findall(r'form method=\"post\" action=\"(.*?)\">',page)
		if(found):
		    print found[0]
		    req = s.post('https://vk.com'+found[0],data={'code':'5048081'})
		    with open('bb','w') as f:
			f.write(req.content)
		
	#    cookies are in session now
	#    cookies.append('remixsslsid=1')
	#    cookies.append('remixflash=11.7.700')
	#    cookies.append('audio_vol=80')
	#    cookies.append('remixdt=-3600')
	#    cookies = '; '.join(cookies)
	#    print cookies
	#	cookies = 'remixdt=-3600; remixflash=11.7.700; audio_vol=80; remixsid='+remixsid+'; remixlang=0; remixrefkey=2680e8e4f5da91753f; remixseenads=2; remixsid6=; remixoldmail=; remixreg_sid=; remixapi_sid=; remixsslsid=1; remixmid=; remixgid=; remixemail=; remixpass=; remixpermit=; remixtst=33950084; remixrec_sid='
		headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0 FirePHP/0.7.2','Referer':'voting.t4log.com/vk'}
		adr = 'https://oauth.vk.com/authorize?client_id=3730319&scope=photos&redirect_uri='+urllib.quote_plus('http://voting.t4log.com/vk')+'&response_type=code'
		print adr

		req = s.get(adr,headers=headers)
	#    req.add_unredirected_header('Host','oauth.vk.com')
	#    req.add_unredirected_header('Referer','http://voting.t4log.com')
	#	with open('aa','w') as f:
	#	    f.write(req.content)
		auth_params = json.loads(req.content)
		#print auth_params
		return {'token':auth_params['access_token'], 'expires':auth_params['expires_in']}
	else:
		return False;
	
def sendfile(upload_url, auth_token, s,file):
	files = {'file1':file}
	req = s.post(upload_url,files=files)
	upload_response = json.loads(req.text)
	req = s.get('https://api.vk.com/method/photos.save?aid=176102106&gid=55311180&server='+str(upload_response['server'])+'&photos_list='+str(upload_response['photos_list'])+'&hash='+str(upload_response['hash'])+'&caption=testcaption&access_token='+auth_token)
	res = json.loads(req.text)
	#print res
	if(res):
		return res['response'][0]['src_big']
	else:
		return False
	
def updatemysql(mysql_con, src, id):
	cur = mysql_con.cursor()
	cur.execute("UPDATE violations_protocols SET vkontakte = 1, vkontakte_path=%s WHERE id=%s", (src,id))
	return cur.rowcount > 0

def markfailed(mysql_con, id, reason):
	cur = mysql_con.cursor()
	cur.execute("UPDATE violations_protocols SET vkontakte = -1, vkontakte_path=%s WHERE id=%s", (reason,id))
	return cur.rowcount > 0

con = False
try:
	con = mdb.connect(host='localhost',user='voting',passwd='Voting#98',db='voting',charset='utf8',use_unicode=True)
	s = requests.Session()
	while(True):
		auth = authentificate(s)
		print auth
		if(auth):
			auth_token = auth['token']
			expires_in = int(time.time()) + auth['expires']

			while(time.time() < expires_in):
				cur = con.cursor(mdb.cursors.DictCursor)
				cur.execute("SELECT id, path FROM violations_protocols WHERE vkontakte=0")
				if(cur.rowcount > 0):
					req = s.get('https://api.vk.com/method/photos.getUploadServer?aid=176102106&gid=55311180&save_big=1&access_token='+auth_token)
					upload_params = json.loads(req.text)
					rows = cur.fetchall()
					for row in rows:
						try:
							with open(row['path'],'rb') as f:
								res = sendfile(upload_params['response']['upload_url'],auth_token, s,f)
								if(res):
									updatemysql(con,res,row['id'])
						except IOError, e:
							print "IOError %d: %s %s" %(e.errno, e.strerror, e.filename)
							markfailed(con,row['id'],e.strerror)
				time.sleep(5)
		else:
			time.sleep(10)
				
except mdb.Error,e:
	print "Mysql Error %d: %s" % (e.args[0], e.args[1])
	
finally:
	if(con):
		con.close()