<?php defined('SYSPATH') or die('No direct script access.');
return array(
	'Username' => 'Логин',
	'Login'=>'Логин',
	'Password' => 'Пароль',
	'Repeat password' =>'Повторите пароль',
	'NOT_LOGGED_IN' =>'Пожалуйста войдите в систему',
	'Full name' =>'Полное имя',
	'Show your full name' =>'Показывать Ваше имя',
	'Show your name' =>'Показывать Ваше имя',
	'Region'=>'Область',
    'Regions'=>'Области',
	'Okrug' => 'Мажоритарный округ',
    'Okrugs' => 'Округа',
	'Comission' =>'Избирательный участок',
	'Sign in' =>'Войти',
    'You can %s if you have an account' => 'Вы можете %s если у Вас уже есть учетная запись',
    'sign in' => 'войти',
	'Sign up' => 'Зарегистрироваться',
	'Please register'=>'Пожалуйста зарегистрируйтесь',
	'Or' => 'Или',
	'if you have account'=>'если у вас есть учетная запись',
	'Parties'=>'Партии',
	'parties'=>'партии',
	'Party'=>'Партия',
	'party'=>'партия',
	
	'Deputies'=>'Депутаты',
	'deputies'=>'депутаты',
	'Deputy'=>'Депутат',
	'deputy'=>'депутат',
	'You need to login first'=>'Вам нужно залогинится прежде чем вы сможете пользоваться всеми возможностями системы',
	'Register new user'=>'Зарегистрировать новую учетную запись',
	'Forgot password'=>'Забыл пароль',
	'Description'=>'Описание',
	'description'=>'описание',
	'Violation type'=>'Тип нарушения',
	'Upload protocol'=>'Загрузить фото протокола',
	'Violation photo'=>'Фото нарушения',
	'Violation video'=>'Видео нарушения',
	'Voting control'=>'Контроль выборов',
    'Inform'=>'Сообщить',
    'Are you sure you wan\'t to delete this file?'=>'Вы уверены что хотите удалить этот файл?',
    'Save votes' => 'Сохранить голоса',
    'No such protocol' => 'Нет такого протокола',
    'This protocol belongs to another user' => 'Этот протокол загружен другим пользователем',
	
	'Back to main'=>'Назад на главную',
	'Data was saved successfully'=>'Данные были сохранены',
	'Inform about violation'=>'Сообщить о нарушении',
	'Inform about one more violation'=>'Сообщить еще об одном нарушении',
	'Upload photo'=>'Загрузить фото',
	'Upload video'=>'Загрузить видео',
	'Enter votes for parties'=>'Введите количество голосов, отданные за партии (по протоколу)',
	'votes'=>'голосов',

    'Choose party' =>'Выберите партию',
    'Percentage'=>'В процентах',
    'Absolute values'=>'Абсолютные значения',
    'Choose violation type'=>'Выберите тип нарушений',
    'Choose numbers type'=>'Выбирите тип значений',
    'all'=>'все',
    
    'Error happened'=>'Произошла ошибка',
    'Sorry, but no such page. Please check if page address is written correctly'=>'�?звините, запрошенная страница отсутствует. Проверьте пожалуйста правильность написания адреса',

    'Do control elections' => 'Контролируй выборы',
    'Ukraine Laws'=>'Законодательство Украины',
    'For request makers'=>'Для подающих жалобу',
    'Previous violations'=>'Прошлые нарушения',
    'Lawyer advice'=>'Советы юриста',
    'Global stats'=>'Общая статистика',
    'Violations by subregion'=>'Нарушения по округам',
    'Votes by subregion'=>'Голоса по округам',

    'Admin panel' => 'Админпанель',
    'Add party' => 'Добавить партию',
    'Add oik' => 'Добавить округ',
    'How it works' => 'Как это работает',
    'Add protocol' => 'Добавить протокол',
    'Note violation' => 'Зафиксировать нарушение',
    'Contacts' => 'Контакты',
    'About' => 'О нас',
    'For observers' => 'Наблюдателям',
    'Register as observer' => 'Зарегистрироваться наблюдателем',
    'Forgot password' => 'Забыли пароль',
    'Login' => 'Логин',
    'Password' => 'Пароль',
    'Logout' => 'Выйти',
    'IK' => 'Избирательный участок',

    'Unconfirmed observer'=>'Неподтвержденный наблюдатель',
    'Сonfirmed observer'=>'Подтвержденный наблюдатель',
    'Observer from party'=>'Наблюдатель от партии',
    "You don't have rights to access this page"=>'У вас недостаточно прав для просмотра этой страницы',
    'This page is not accessible from mobile devices'=>'Страница не доступна для мобильных клиентов',
    'Register party observer' => 'Зарегистрировать наблюдателя от партии',
    'Are you sure you want to delete this observer' => 'Вы уверены что хотите удалить этого наблюдателя',
    'You have no permision to delete this observer' => 'У Вас недостаточно прав что бы удалить этого наблюдателя',
    "Observer with this id doesn't exist" => 'Наблюдателя с таким id не существует',
    'Error happened during request to server' => 'Произошла ошибка при обращении к серверу',
    'No observer with such id' => 'Наблюдателя с таким id не существует',
    'Party admin' => 'Администратор партии',
    'Manage party observers' => 'Управление наблюдателями от партии',
    'Confirm' => 'Подтвердить',
    'Are you sure you want to confirm this observer' => 'Вы уверены что хотите подтвердить, что этот наблюдатель от Вашей партии?',
    "You can't confirm this user as party admin" => 'Вы не можете "подтвердить" этого пользователя как наблюдателя от партии',
    'Select, how will be confirmed your identity' => 'Выберите, в каком режиме Вы хотите зарегистрироваться и как подтвердить вашу идентичность',
    'Unconfirmed observer, your voting data will be less trusted' => 'Неподтвержденный наблюдатель, ваши данные будут идти с меньшим приоритетом и достоверностью',
    'Сonfirmed observer, your voting data will be used first when building statistics' => 'Подвтержденный сканом пасспорта и телефоном, ваши данные будут использованы для построения независимой статистики',
    'Observer from party, your voting data will be used by parties for statistics and can be used for general statistics also' => 'Наблюдатель от партии. Ваши данные будут использованы для построения как статистики для партий, так и общей статистики',
    'Add observer' => 'Добавить наблюдателя',
    'Phone' => 'Телефон',
    'phone' => 'телефон',
    'Party observers' => 'Наблюдатели от партии',
    'Error requesting server for validating form' => 'Ошибка при запросе к серверу для валидации данных',
    'Username is already used' => 'Этот логин уже занят',
    'Email is already used' => 'Этот email уже занят',
    'Edit' => 'Редактирвать',
    'Delete' => 'Удалить',
    'Wrong captcha' => 'Неверно введены символы с картинки',
    'Enter symbols from image' => 'Введите символы с картинки, для подтверждения, что вы человек',
    'You need to upload passport photo' => 'Вам необходимо загрузить скан-копию или фото первой страницы пасспорта в этом режиме подтверждения личности',

    ':field must be a digit' => 'поле :field должно состоять из цифр',
   	':field must be a email address' => 'поле :field должно быть email адресом',
   	':field must contain a valid email domain' =>'поле :field должно иметь валидный почтовый домен',
   	':field must equal :param2' => 'поле :field должно совпадать :param2',
   	':field must be at least :param2 characters long' => 'поле :field должно быть как минимум :param2 символов длиной',
   	':field must not exceed :param2 characters long' => 'поле :field не должно превышать :param2 символов по длине',
   	':field must not be empty' => 'поле :field не должно быть пустым',
   	':field must be numeric' => 'поле :field должно быть числовым',
   	':field must be a phone number' => 'поле :field должно быть номером телефона',
   	':field must be within the range of :param2 to :param3' => 'значение поля :field должно быть от :param2 до :param3',
   	':field does not match the required format' => 'поле :field не соответствует требуемому формату',
   	':field :param2 is already used' => 'это значение для поля :field уже занято',
	"password and confirmation doesn't match" => 'пароль и подтверждение не совпадают',
	':field must be the same as :param3' => 'поле ":field" и ":param3" должны совпадать',
	//':field must be the same as :param2' => 'пароль и подтверждение пароля не совпадают',
	'password'=>'пароль',
	'password confirm'=>'подтверждение пароля',
	'password_сonfirm'=>'подтверждение пароля',
	'more...' => 'подробнее...',
    'here' => 'тут',
    'By region/okrug/comission' => 'По областям/округам/участкам',
    'For parties and candidates' => 'Для партий и кандидатов',
    'If you notice an error, please notify' => 'Если Вы заметили ошибку, пожалуйста сообщите',
    'Sorry, registration from mobile devices are nt available at the moment' => 'Извините, регистрация с мобильных устройств пока не доступна. В течении суток все будет работать, пожалуйста зарегистрируйтесь с компьютера',
    'Upload passport photocopy' => 'Загрузите фото/скан первой страницы пасспорта',
    'Thank you for successfull registration' => 'Спасибо за успешную регистрацию',
    'Elections control' => 'Контролируй выборы',
);
?>