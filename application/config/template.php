<?php defined('SYSPATH') or die('No direct access allowed.');


return array(

	'site_link'		=> 'uavibor.com',	// Название сайта. НУЖНО ИЗМЕНИТЬ

	'template_dir' 		=> 'template',		// Папка в application/views, содержащая шаблоны шаблона :)

	'target'		=> 'template',		// Путь к главному VIEW из папки шаблона для загружаемой страницы

	'media_dir'		=> 'assets',		// Папка в корне, откуда будут грузиться CSS и JS файлы

);
