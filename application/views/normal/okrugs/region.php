<div class="block">
    <h2>Okrugs list in <?=$region?></h2>
    <table class="admin_table">
        <?
        foreach($items as $okrug)
        {
            echo '<tr><td fieldname="name">'.$okrug->name.'</td><td><a href="/deputies/index/'.$okrug->id.'">deputies</a></td><td><a href="/comissions/index/'.$okrug->id.'">comissions</a></td>
            <td class="op"><a href="'.$okrug->id.'" class="editlink">edit</a></td><td class="op"><a href="'.$okrug->id.'" class="deletelink">delete</a></td></tr>';
        }
        ?>
    </table>
    <div class="add-frame">
        <a href="#" id="add-item">Add okrug</a>
    </div>
</div>
    <div id="editdlg" class="hidden_edit_dlg">
        <form>
            <label>Name: <input type="text" name="name" value="" id="name_value" class="formfield"></label>
            <input type="hidden" name="region_id" value="<?=$region_id?>" class="formfield">
        </form>
    </div>
    <script>
        var row_template = '<td fieldname="name">_name_</td><td><a href="/deputies/index/_id_">deputies</a></td><td><a href="/comissions/index/_id_">comissions</a></td>';
    </script>
