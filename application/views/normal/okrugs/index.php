<div class="block">
    <table class="admin_table">
        <?
        foreach($items as $okrug)
        {
            echo '<tr><td fieldname="region_id" fieldval="'.$okrug->region_id.'">'.$regions[$okrug->region_id].'</td><td fieldname="name">'.$okrug->name.'</td>
        	<td><a href="/deputies/index/'.$okrug->id.'">deputies</a></td><td><a href="/comissions/index/'.$okrug->id.'">comissions</a></td>
            <td><a href="'.$okrug->id.'" class="editlink">edit</a></td><td><a href="'.$okrug->id.'" class="deletelink">delete</a></td></tr>';
        }
        ?>
    </table>
    <div class="add-frame">
        <a href="#" id="add-item">Add okrug</a>
    </div>
</div>
    <div id="editdlg" class="hidden_edit_dlg">
        <form>
            <label>Region: <?=form::select('region_id',$regions,null,array('id'=>'region_id_value','class'=>'formfield'))?></label>
            <label>Name: <input type="text" name="name" value="" id="name_value" class="formfield"></label>
        </form>
    </div>
    <script>
        var row_template = '<td fieldname="region_id" fieldval="_region_id_">_region_id+val_</td><td fieldname="name">_name_</td><td><a href="/deputies/index/_id_">deputies</a></td><td><a href="/comissions/index/_id_">comissions</a></td>';
    </script>
