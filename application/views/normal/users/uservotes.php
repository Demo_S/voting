<div id="mainpart">
	<?=__('Parties')?>
    <table class="admin_table">
        <?
        foreach($parties as $party)
        {
			echo '<tr><td>'.$party->name.'</td><td>'.$party->votes.'</td></tr>';
        }
        ?>
    </table>
	<?= __('Deputies') ?>
	    <table class="admin_table">
        <?
        foreach($deputies as $deputy)
        {
			echo '<tr><td>'.$deputy->name.'</td><td>'.$deputy->votes.'</td></tr>';
        }
        ?>
    </table>

	<?= __('Violations') ?>
	    <table class="admin_table">
        <?
        foreach($violations as $violation)
        {
			echo '<tr><td>'.$violation->name.'</td><td>'.$violation->ts.'</td></tr>';
        }
        ?>
    </table>

</div>
