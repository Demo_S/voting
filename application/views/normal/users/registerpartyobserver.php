<div id="mainpart">
<h1><?=__('Register party observer')?></h1>

<div class="user_fields bootstrap">
 <?php
if (isset($errors) && $errors){
	echo View::factory('error',array('error'=>$errors))->render();
}
 ?>
 <?=form::open(null,array('role'=>'form','class'=>'form-horizontal','id'=>'observer_form' ))?>
    <?=form::hidden('register_form_sent',1);?>
    <div class="form-group">

	<?=form::label('username', __('Login'), ['class'=>'col-sm-2'])?>
	<div class="col-sm-5">
	<?=form::input('username',$user->username,array('class'=>'form-control','id'=>'input_username'))?>
    <?=misc::format_validation_error_if_any($validation,'username')?>
	</div>
    </div>
    <div class="form-group">
	<?=form::label('password', __('Password'),['class'=>'col-sm-2'])?>
	<div class="col-sm-5">
	<?=form::password('password','',['class'=>'form-control'])?> <div class="password_tip"><?=__('6 or more symbols')?></div>
        <?=misc::format_validation_error_if_any($validation,'password')?>
	</div>
    </div>
    <div class="form-group">
	<?=form::label('password_confirm', __('Repeat password'),['class'=>'col-sm-2'])?>
	<div class="col-sm-5">
	<?=form::password('password_confirm','',['class'=>'form-control'])?><div class="password_tip"><?=_("Passwords don't match")?></div>
        <?=misc::format_validation_error_if_any($validation,'password_confirm')?>
	</div>
    </div>
    <div class="form-group">
	<?=form::label('email', 'Email',['class'=>'col-sm-2'])?>
	<div class="col-sm-5">
	<?=form::input('email',$user->email,['class'=>'form-control','id'=>'input_email'])?>
                <?=misc::format_validation_error_if_any($validation,'email')?>
	</div>
    </div>
    <div class="form-group">
	<?=form::label('fullname', __('Full name'),['class'=>'col-sm-2'])?>
	<div class="col-sm-5">
	<?=form::input('fullname',$user->fullname,['class'=>'form-control'])?>
                <?=misc::format_validation_error_if_any($validation,'fullname')?>
	</div>
    </div>

    <div class="form-group">
    <?=form::label('region_id', __('Region'),['class'=>'col-sm-2'])?>
	<div class="col-sm-5">
	<?$regions[0] = '';ksort($regions);?>
	<?=form::select('region_id',$regions, $user->region_id ?: 0,array('id'=>'regions','class'=>'form-control'))?>
                <?=misc::format_validation_error_if_any($validation,'region_id')?>
	</div>
    </div>
    <div class="form-group">
        <?=form::label('okrug_id', __('Okrug'),['class'=>'col-sm-2'])?>
	<div class="col-sm-5">
        <?=form::select('okrug_id',$okrugs, $user->okrug_id,array('id'=>'okrugs','class'=>'form-control','disabled'=>'disabled'))?><!--Load via ajax-->
                <?=misc::format_validation_error_if_any($validation,'okrug_id')?>
	</div>
    </div>
    <div class="form-group">
	<?=form::label('comission_id', __('Comission'),['class'=>'col-sm-2'])?>
	<div class="col-sm-5">
	<?=form::select('comission_id',$comissions,$user->comission_id,array('id'=>'comissions','class'=>'form-control', 'disabled'=>'disabled'))?>
                <?=misc::format_validation_error_if_any($validation,'comission_id')?>
	</div>
    </div>
    <div class="form-group">
    <?=form::label('phone', __('Phone'),['class'=>'col-sm-2'])?>
	<div class="col-sm-5">
        <?=form::input('phone',$user->phone,['class'=>'form-control'])?>
                <?=misc::format_validation_error_if_any($validation,'phone')?>
	</div>
    </div>
    <div class="form-group">
	<?=form::submit('submit', __('Save'),array('class'=>"btn btn-default"))?>
    </div>
<?=form::close()?>
</div>
<script>
    $(function(){
        $('#regions').change(function(e){console.log('change');
            var region_id = $('#regions option:selected').val();
            $.get('/users/okrugslist/'+region_id,null,function(res){
		var html_string = '';
		for(var okrug_id in res){
		    html_string += '<option value="'+okrug_id + '">'+res[okrug_id]+'</option>';
		}
		$('#okrugs').html(html_string).prop('disabled',false).change();
            },'json');
        });
        $('#okrugs').change(function(e){
            var okrug_id = $('#okrugs option:selected').val();
            $.get('/users/comissionlist/'+okrug_id,null,function(res){
		var html_string = '';
		for(var comission_id in res){
		    html_string += '<option value="'+res[comission_id] + '">'+res[comission_id]+'</option>';
		}
                $('#comissions').html(html_string).prop('disabled',false);
            },'json');
        });
	if(<?=count($okrugs)?>){
	    $('#okrugs').prop('disabled',false);
	}
	if(<?=count($comissions)?>){
	    $('#comissions').prop('disabled',false);
	}
    });
</script>
</div>