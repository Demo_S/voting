<div id="mainpart">
<h1><?=__('Party observers')?> "<?=Auth::instance()->get_user()->party->name?>"</h1>

<div class="user_fields bootstrap">
 <?php
if (isset($errors) && $errors){
	echo View::factory('errors',array('errors'=>$errors))->render();
}
 ?>
        <table class="admin_table table table-striped">
            <tr><th>id</th><th><?=__('username')?></th><th><?=__('Full name')?></th><th><?=__('Region')?></th><th><?=__('Okrug')?></th><th><?=__('Comission')?></th><th></th><th></th></tr>
            <?
            foreach($observers as $observer)
            {
                echo '<tr><td>'.$observer->id.'</td><td>'.$observer->username.'</td>
                        <td>'.$observer->fullname.'</td>
                        <td>'.$observer->region->name.'</td>
                        <td>'.$observer->okrug->name.'</td>
                        <td>'.$observer->comission_id.'</td>
                        <td class="op">'.(($observer->confirmed_state == Controller_Users::CONFIRMATION_STATUS_PARTYOBSERVERREQUEST) ?
                                '<a href="'.$observer->id.'" class="confirmlink btn btn-sm btn-default">'.__('Confirm').'</a>':
                                Html::anchor('users/editpartyobserver/'.$observer->id,__('Edit'),['class'=>'btn btn-sm btn-default'])).'</td>
                        <td class="op"><a href="'.$observer->id.'" class="deletelink btn btn-default btn-sm">'.__('Delete').'</a></td></tr>';
            }
            ?>
        </table>
        <div class="add-frame">
            <?=Html::anchor('users/addpartyobserver',__('Add observer'),['class'=>'btn btn-default'])?>
        </div>
        <script>
            $(function(){
                $('a.deletelink').click(function(e){console.log('deletelink');	
                    e.preventDefault();
                    if(confirm('<?=__('Are you sure you want to delete this observer')?>')){
                        var link = $(this);
                        $.ajax({
                            'url':'<?=URL::base()?>'+ 'users/deletepartyobserver/' + link.attr('href'),
                            dataType:'json'
                        }).success(function(response){
                            if(response.success){
                                link.parent().parent().remove();
                            }
                            else{
                                alert(response.error);
                            }
                        }).error(function(){
                             alert('<?=__('Error happened during request to server')?>')
                        });
                    }
                });
                $('a.confirmlink').click(function(e){
                    e.preventDefault();
                    if(confirm('<?=__('Are you sure you want to confirm this observer')?>')){
                        var link = $(this);
                        var user_id = link.attr('href');
                        $.ajax({
                            'url':'<?=URL::base()?>'+ 'users/confirmpartyobserver/' + user_id,
                            dataType:'json'
                        }).success(function(response){
                            if(response.success){
                                link.parent().html('<a href="<?=URL::base()?>users/editpartyobserver/'+user_id+'" class="btn btn-sm btn-default"><?=__('Edit')?></a>');
                            }
                            else{
                                alert(response.error);
                            }
                        }).error(function(){
                             alert('<?=__('Error happened during request to server')?>')
                        });
                    }
                });
            });
        </script>
 </div>
</div>