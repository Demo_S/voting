<div id="mainpart">
<h1><?=__('Please register')?></h1>
<h3><?= sprintf(__('You can %s if you have an account'),Html::anchor('users/login',__('sign in')))?></h3>
<span class="small"><?=__('If you notice an error, please notify')?> <a href="mailto:uavibor@gmail.com">uavibor@gmail.com</a></span>
<div class="user_fields bootstrap">
 <?php  
if (isset($errors) && $errors){
	echo View::factory('error',array('error'=>$errors))->render();
}
 ?>
 <?=form::open('users/register', ['class'=>'form-horizontal','role'=>'form','enctype'=>'multipart/form-data','id'=>'reg_form'])?>
<div class="form-group">
	<?=form::hidden('register_form_sent',1)?>
	<?=form::label('username', __('Login'),array('class'=>'col-sm-3'))?>
    <div class="col-sm-5">
	<?=form::input('username',Arr::get($user,'username'),['class'=>'form-control','id'=>'input_username'])?>
	<?=misc::format_validation_error_if_any($validation,'username')?>
    </div>
</div>
<div class="form-group">
	<?=form::label('password', __('Password'),array('class'=>'col-sm-3'))?>
    <div class="password col-sm-5">
	<?=form::password('password','',['class'=>'form-control'])?> <div class="password_tip"><?=__('6 or more symbols')?></div>
	<?=misc::format_validation_error_if_any($validation,'password')?>
    </div>
</div>
<div class="form-group">
	<?=form::label('password_confirm', __('Repeat password'),array('class'=>'col-sm-3'))?>
    <div class="password_confirm col-sm-5">
	<?=form::password('password_confirm','',['class'=>'form-control'])?><div class="password_tip"><?=_("Passwords don't match")?></div>
	<?=misc::format_validation_error_if_any($validation,'password_confirm')?>
    </div>
</div>
<div class="form-group">
	<?=form::label('email', 'Email',array('class'=>'col-sm-3'))?>
    <div class="col-sm-5">
	<?=form::input('email',Arr::get($user,'email'),['class'=>'form-control','id'=>'input_email'])?>
	<?=misc::format_validation_error_if_any($validation,'email')?>
    </div>
</div>
<div class="form-group">
    <?=form::label('fullname', __('Full name'),array('class'=>'col-sm-3'))?>
    <div class="col-sm-5">
    <?=form::input('fullname',Arr::get($user,'fullname'),['class'=>'form-control'])?>
	<?=misc::format_validation_error_if_any($validation,'fullname')?>
    </div>
</div>

<div class="form-group">
    <?=form::label('region_id', __('Region'),array('class'=>'col-sm-3'))?>
    <div class="col-sm-5">
	<?$regions[0] = ''; ksort($regions);?>
	<?=form::select('region_id',$regions, Arr::get($user,'region_id'),array('id'=>'regions','class'=>'form-control'))?>
	<?=misc::format_validation_error_if_any($validation,'region_id')?>
    </div>
</div>
<div class="form-group">
        <?=form::label('okrug', __('Okrug'),array('class'=>'col-sm-3'))?>
    <div class="col-sm-5">
        <?=form::select('okrug_id',$okrugs, Arr::get($user,'okrug_id'),array('id'=>'okrugs','class'=>'form-control','disabled'=>'disabled'))?><!--Load via ajax-->
	<?=misc::format_validation_error_if_any($validation,'okrug_id')?>
    </div>
</div>
<div class="form-group">
    <?=form::label('comission', __('Comission'),array('class'=>'col-sm-3'))?>
    <div class="col-sm-5">
    <?=form::select('comission_id',$comissions,Arr::get($user,'comission_id'),array('id'=>'comissions','class'=>'form-control','disabled'=>'disabled'))?>
	<?=misc::format_validation_error_if_any($validation,'comission_id')?>
    </div>
</div>
<div class="form-group" id="group-phone">
	<?=form::label('phone',__('Phone'),['class'=>'col-sm-3'])?>
        <div class="col-sm-5">
	    <?=form::input('phone',Arr::get($user,'phone'),['class'=>'form-control'])?>
	<?=misc::format_validation_error_if_any($validation,'phone')?>
	</div>
</div>
<div class="form-group">
        <?=form::label('confirmed_state',__('Select, how will be confirmed your identity'),['class'=>'col-sm-3'])?>
    <div class="col-sm-5" id="confirmed_state_wrapper">
	<table>
	<tr>
        <td><?=form::radio('confirmed_state',Controller_Users::CONFIRMATION_STATUS_UNCONFIRMED,Arr::get($user,'confirmed_state') == Controller_Users::CONFIRMATION_STATUS_UNCONFIRMED)?></td>
	<td><label><?=__('Unconfirmed observer, your voting data will be less trusted')?></label></td>
	</tr>
	<tr>
        <td><?=form::radio('confirmed_state',Controller_Users::CONFIRMATION_STATUS_PASSPORT,Arr::get($user,'confirmed_state') == Controller_Users::CONFIRMATION_STATUS_PASSPORT)?></td>
	<td><label><?=__('Сonfirmed observer, your voting data will be used first when building statistics')?></label></td>
	</tr>
	<tr>
        <td><?=form::radio('confirmed_state',Controller_Users::CONFIRMATION_STATUS_PARTYOBSERVERREQUEST,Arr::get($user,'confirmed_state') == Controller_Users::CONFIRMATION_STATUS_PARTYOBSERVERREQUEST)?></td>
	<td><label><?=__('Observer from party, your voting data will be used by parties for statistics and can be used for general statistics also')?></label></td>
	</tr>
	</table>
    </div>
</div>

<div class="form-group initially-hidden" id="group-passport"><?=form::label('passport', __('Upload passport photocopy'),array('class'=>'col-sm-3'))?>
        <div class="col-sm-5"><?=form::file('passport',['class'=>'form-control'])?></div>
</div>
    <div class="form-group initially-hidden" id="group-party"><?=form::label('party', __('Party'),array('class'=>'col-sm-3'))?>
            <div class="col-sm-5"><?=form::select('party_id',$parties, Arr::get($user,'party_id'),array('id'=>'regions','class'=>'form-control'))?></div>
	<?=misc::format_validation_error_if_any($validation,'party_id')?>
    </div>

    <div class="form-group "><?=form::label('captcha',__('Enter symbols from image'),array('class'=>'col-sm-3'))?>
            <div class="col-sm-5"><?=$captcha.form::input('captcha','',array('class'=>'form-control'))?></div>
    </div>
<div class="form-group">
	<?=form::submit('submit', __('Sign up'),['class'=>'btn btn-default'])?>
</div>
<?=form::close()?>
</div>
<script>
    $(function(){
	$('#regions').change(function(e){
	    var region_id = $('#regions option:selected').val();
	    $.get('/users/okrugslist/'+region_id,null,function(res){
		var html_string = '';
		for(var okrug_id in res){
		    html_string += '<option value="'+okrug_id + '">'+res[okrug_id]+'</option>';
		}
		$('#okrugs').html(html_string).prop('disabled',false).change();
	    },'json');
	});
	$('#okrugs').change(function(e){
	    var okrug_id = $('#okrugs option:selected').val();
	    $.get('/users/comissionlist/'+okrug_id,null,function(res){
		var html_string = '';
		for(var comission_id in res){
		    html_string += '<option value="'+res[comission_id] + '">'+res[comission_id]+'</option>';
		}
		$('#comissions').html(html_string).prop('disabled',false);
	    },'json');
	});
    if(<?=count($okrugs)?>){//in case of returning to same form in case of error, prefil and preset okrug/comission selects
        $('#okrugs').prop('disabled',false);
    }
    if(<?=count($comissions)?>){
        $('#comissions').prop('disabled',false);
    }

	$('#confirmed_state_wrapper input').change(function(){
	    var $this = $(this);
	    if($this.prop('checked')){
		switch(parseInt($this.val())){
		    case <?=Controller_Users::CONFIRMATION_STATUS_UNCONFIRMED?>:
			$('#group-passport').hide();
			$('#group-party').hide();
		    break;
		    case <?=Controller_Users::CONFIRMATION_STATUS_PASSPORT?>:
			$('#group-passport').show();
			$('#group-party').hide();
		    break;
		    case <?=Controller_Users::CONFIRMATION_STATUS_PARTYOBSERVERREQUEST?>:
			$('#group-passport').hide();
			$('#group-party').show();
		    break;
		}
	    }
	});
	$('#confirmed_state_wrapper input').change();//initial setup
	$('.form-control').change(function(){
	    $(this).next('span').remove();
	});
    });
</script>
</div>