<div id="mainpart">
<h2><?=__('NOT_LOGGED_IN')?></h2>

<?php if (isset($error)): ?>
	<div class="error" style="color: red;">
		<?=$error?>
	</div>
<?php endif; ?>

<?=form::open('users/login')?>
	<?=form::hidden('login_form_sent',1);?>
	<table><tr><td>
	<?=form::label('username', __('Username:'))?>
	</td><td>
	<?=form::input('username', $username)?>
	</td></tr><tr><td>
	<?=form::label('password', __('Password:'))?>
	</td><td>
	<?=form::password('password')?>
	</td></tr><tr><td colspan="2">
	<?=form::submit('submit', __('Login'))?>
	</td></tr>
	</table>
<?=form::close()?>
<br />
	<?=HTML::anchor('users/register',__('Register new user'))?> :: <?/*HTML::anchor('users/forgot',__('Forgot password'))*/?>
</div>