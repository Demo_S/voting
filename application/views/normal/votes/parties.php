<div id="mainpart">
    <?if(count($errors)):?>
    <div class="errors">
	<?foreach($errors as $error):?>
	    <div><?=$error?></div>
	<?endforeach?>
    </div>
    <?endif?>
    <div class="parties input_list">
        <h4></p><?=__('Enter votes for parties')?>:</h4>
        <form method="POST" enctype="multipart/form-data">
            <fieldset>
                <?
                    foreach($parties as $party_id=>$party_name):
                ?>
                <label><span><?=$party_name?>:</span> <input type="text" name="parties[<?=$party_id?>]" value="<?=Arr::get($parties_votes,$party_id)?>"> <?=__('votes')?></label>
                <? endforeach;?>
            </fieldset>
            <div><label for="protocol"><?=__('Upload protocol')?></label><input type="file" id="protocol" name="parties_protocol"></div>
            <input type="hidden" name="parties_sent" value="1">
            <input type="submit" name="submit_deputies" value="<?=__('Save votes')?>">
        </form>
        <div id="protocols" class="media_files">
            <?
            foreach($protocols as $id=>$path) {
                $pathinfo = pathinfo($path);
                $thumb = misc::thumb_from_path($path);
                echo '<div class="media_file"><a class="protocol_photo" href="/'.$path.'" target="_blank" rel="prettyPhoto">'.Html::image($thumb).'</a> :: <a href="'.$id.'" class="deletelink">'.__('delete').'</a></div>';
            }
            ?>
            <div class="clear"></div>
        </div>
    </div>

</div>
<script type="text/javascript">
    $(function(){
        $('a.deletelink').click(function(e){
            e.preventDefault();
            if(confirm('<?=__('Are you sure you wan\'t to delete this file?')?>'))
            {
                var id = $(this).attr('href');
                var link = $(this);
                $.get('votes/delete_protocol/'+id,function(res){
                    if(res.status == 'FAIL'){
                        alert('<?=__('error deleting file')?>' + res.error);
                    }
                    else if(res.status == 'OK'){
                        link.parent('div').remove();
                    }
                })
            }
        });
        $("a[rel='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
    })
</script>