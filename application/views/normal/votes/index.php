<div>
	<?if($violation_posted){
		if($errors){
			echo View::factory('errors',array('errors'=>$errors))->render();
		}
		else{
			echo '<div>'.__('Data was saved successfully').'</div>';
		}
	}?>

        <form method="POST">
    <div class="parties input_list">
        <h4></p><?=__('Enter votes for parties:')?></h4>
            <fieldset>
                <?
                    foreach($parties as $party_id=>$party_name):
                ?>
                <label><span><?=$party_name?>:</span> <input type="text" name="parties[<?=$party_id?>]"> <?=__('votes')?></label>
                <? endforeach;?>
            </fieldset>
    </div>
    <div class="deputies input_list">
        <h4></p><?=__('Enter votes for deputies:')?></h4>
            <fieldset>
                <?
                    foreach($deputies as $deputy_id=>$deputy_name):
                ?>
                <label><span><?=$deputy_name?>:</span> <input type="text" name="deputies[<?=$deputy_id?>]"> <?=__('votes')?></label>
                <? endforeach;?>
            </fieldset>
    </div>
            <input type="submit">
        </form>
    <a href="/uploads/index"><?=__('Upload protocols/violations')?></a>
</div>