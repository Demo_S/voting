<div>
    <div class="deputies input_list">
        <h4></p><?=__('Enter votes for deputies:')?></h4>
        <form method="POST" enctype="multipart/form-data">
            <fieldset>
                <?
                    foreach($deputies as $deputy_id=>$deputy_name):
                ?>
                <label><span><?=$deputy_name?>:</span> <input type="text" name="deputies[<?=$deputy_id?>]" value="<?=$deputies_votes[$deputy_id]?>"> <?=__('votes')?></label>
                <? endforeach;?>
            </fieldset>
            <div><label for="protocol"><?_('Upload protocol')?></label><input type="file" id="protocol" name="votingprotocol"></div>
            <input type="hidden" name="deputies_sent" value="1">
            <input type="submit" name="submit_deputies" value="<?=__('Save votes')?>">
        </form>
        <div id="protocols" class="media_files">
            <?
            foreach($protocols as $protocol) {
                $pathinfo = pathinfo($path);
                $thumb = misc::thumb_from_path($pathinfo);
                echo '<div><a href="'.$path.'" target="_blank" rel="prettyPhoto"><img src="'.$thumb.'"</a> :: <a href="'.$id.'" class="deletelink">'.__('delete').'</a></div>';
            }
            ?>
        </div>
    </div>

</div>
<script type="text/javascript">
    $(function(){
        $('a.deletelink').click(function(e){
            e.preventDefault();
            if(confirm('<?=__('Are you sure you wan\'t to delete this file?')?>'))
            {
                var id = $(this).attr('href');
                var link = $(this);
                $.get('votes/delete_protocol/'+id,function(res){
                    if(res.status == 'FAIL'){
                        alert('<?=__('error deleting file: ')?>' + res.error);
                    }
                    else if(res.status== 'OK'){
                        link.parent('div').remove();
                    }
                })
            }
        });
        $("a[rel='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
    })
</script>