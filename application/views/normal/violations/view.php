<?php
/**
 * Created by JetBrains PhpStorm.  Author: Демо_С.
 * Date: 25.10.12  21:46
 * view single violation (with possibly several media files)
 */
 ?>
<div id="mainpart">
    <h3><?=__('View violation data')?></h3>
        <div>
            <h6><?=__('Date')?></h6>
            <p><?=date('Y-m-d H:i:s',$violation->ts)?></p>
        </div>
        <div>
            <h6><?=__('Description')?></h6>
            <p><?=$violation->description?></p>
        </div>
        <div>
            <h6><?=__('Violation type')?></h6>
            <?=$violation->violation_type_name?>
        </div>
    <div id="violationmedia">

        <?
        foreach($files as $file){
            $pathinfo = pathinfo($file);
            $thumb = misc::thumb_from_path($pathinfo);
            echo '<div><a href="'.$file.'" target="_blank" rel="prettyPhoto"><img src="'.$thumb.'"></a></div>';
        }
        ?>
    </div>
    <a href="/violations/edit/<?=$violation->id?>"><?=__('Edit')?></a>
</div>
    <script>
        $(document).ready(function(){
  				$("a[rel='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
		});

    </script>