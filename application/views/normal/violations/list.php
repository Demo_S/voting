<?php
/**
 * Created by JetBrains PhpStorm.  Author: Демо_С.
 * Date: 25.10.12  21:45
 * violations list view
 */
?>
<div id="mainpart">
    <h3><?=__('Violations list')?></h3>
    <table>
    <?
        foreach($violations as $violation){
            echo '<tr><td>'.date('Y-m-d H:i:s',$violation->ts).'</td><td>'.$violation->description.'</td>';
            echo '<td>'.$violation_types[$violation->violation_type].'</td>';
            echo '<td><a href="violations/view/' . $violation->id . '">' . __('view') . '</td>';
            echo '<td> <a href="violations/edit/'.$violation->id.'">'.__('edit').'</a></td></tr>';
        }
    ?>
    </table>
</div>