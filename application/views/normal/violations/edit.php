<?php
/**
 * Created by JetBrains PhpStorm.  Author: Демо_С.
 * Date: 25.10.12  21:47
 * edit single violation. add/remove media files
 */
?>
<div id="mainpart">
    <h3><?=__('Edit violation')?></h3>
    <form method="POST" enctype="multipart/form-data">
        <div>
            <?=form::label('description',__('Description'))?>
            <textarea name="description" id="description" cols="80" rows="5" ><?=$violation->description?></textarea>
        </div>
        <div>
            <?=form::label('violation_type',__('Violation type'))?>
            <?=form::select('violation_type',$violation_types, $violation->violation_type,array('id'=>'violation_type'))?>
        </div>


    <div id="upload_violationphoto">
        <label><?=__('Upload photo')?></label><input type="file" name="violation_photo">
    </div>
    <div id="upload_violationvideo">
        <label><?=__('Upload video')?></label><input type="file" name="violation_video">
    </div>
     <input type="submit" name="violation_sent" value="Сообщить">
    </form>
    <div id="violationmedia">
        <?
        foreach($files as $id=>$path){
            $pathinfo = pathinfo($path);
            $thumb = misc::thumb_from_path($pathinfo);
            echo '<div><a href="'.$path.'" target="_blank" rel="prettyPhoto"><img src="'.$thumb.'"</a> :: <a href="'.$id.'" class="deletelink">'.__('delete').'</a></div>';
        }
        ?>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('a.deletelink').click(function(e){
            e.preventDefault();
            if(confirm('<?=__('Are you sure you wan\'t to delete this file?')?>'))
            {
                var id = $(this).attr('href');
                var link = $(this);
                $.get('violations/deletefile/'+id,function(res){
                    if(res.status == 'FAIL'){
                        alert('<?=__('error deleting file')?>');
                    }
                    else if(res.status == 'OK'){
                        link.parent('div').remove();
                    }
                })
            }
        });
        $("a[rel='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});


    })
</script>