<div id="mainpart">
	<?if($violation_posted){
		if($errors){
			echo View::factory('errors',array('errors'=>$errors))->render();
		}
		else{
			echo '<div>'.__('Data was saved successfully').'</div>';
		}
	}?>

    <h3><?=$violation_posted ? __('Inform about one more violation') : __('Inform about violation')?></h3>
    <form method="POST" enctype="multipart/form-data">
        <div>
            <?=form::label('description',__('Description'))?>
            <textarea name="description" id="description" cols="80" rows="5" ></textarea>
        </div>
        <div>
            <?=form::label('violation_type',__('Violation type'))?>
            <?=form::select('type',$violation_types, null,array('id'=>'violation_type','data-theme'=>"d", 'data-mini'=>"true"))?>
        </div>
    <div id="upload_violationphoto">
        <label><?=__('Upload photo')?></label><input type="file" name="violation_photo">
    </div>
    <div id="upload_violationvideo">
        <label><?=__('Upload video')?></label><input type="file" name="violation_video">
    </div>
     <input type="hidden" name="violation_sent" value=1>
     <input type="submit" name="violation_submit" value="<?=__('Inform')?>">
    </form>

	<div><?=Html::anchor('/',__('Back to main'))?></div>	
</div>