<div class="block">
    <table class="admin_table">
        <? 
        foreach($items as $comission)
        {
            echo '<tr><td fieldname="okrug_id" fieldval="'.$comission->okrug_id.'">'.Arr::get($okrugs,$comission->okrug_id).'</td>
	<td fieldname="name">'.($comission->name?:$comission->bounds).'</td>
	<td fieldname="address">'.$comission->address.'</td>
            <td class="op"><a href="'.$comission->id.'" class="editlink">edit</a></td><td class="op"><a href="'.$comission->id.'" class="deletelink">delete</a></td></tr>';
        }
        ?>
    </table>
    <div class="add-frame">
        <a href="#" id="add-item">Add comission</a>
    </div>
</div>
    <div id="editdlg" class="hidden_edit_dlg">
        <form>
            <label>Okrug: <?=form::select('okrug_id',$okrugs,null,array('id'=>'okrug_id_value','class'=>'formfield'))?></label>
            <label>Name: <input type="text" name="name" value="" id="name_value" class="formfield"></label>
        </form>
    </div>
    <script>
        var row_template = '<td fieldname="okrug_id" fieldval="_okrug_id_">_okrug_id+val_</td><td fieldname="name">_name_</td><td><a href="/deputies/index/_id_">deputies</a>';
    </script>
