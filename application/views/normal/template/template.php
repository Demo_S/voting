<!DOCTYPE html>
<html>
	<head>
		<title>
			<?=$title;?>
		</title>

		<?=$metas?>

		<?=$styles?>

		<?=$scripts?>
	</head>
<?flush()?>
<body>
<div id="main">
	<header>
		<div id="coat_of_arms" class="header_block">
        	    <?=Html::anchor('/',HTML::image("assets/images/coat_of_arms.jpg",array('alt'=>'coat_of_arms')))?>
		</div>
		<div id="slogan" class="header_block">
			<h1><a href="/users/register"><?=__('Do control elections')?></a></h1>
			<h2>Для небезразличных граждан</h2>
			<span>Центр обработки информации о нарушениях в ходе выборов</span>
			<div id="more"><a href="/welcome/details"><?=__('more...')?></a></div>
            <a class="twitter-follow-button"
              href="https://twitter.com/uavibor_com"
              data-show-count="false"
              data-lang="en">
            Follow @uavibor_com
            </a>
            <script type="text/javascript">
            window.twttr = (function (d, s, id) {
              var t, js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src= "https://platform.twitter.com/widgets.js";
              fjs.parentNode.insertBefore(js, fjs);
              return window.twttr || (t = { _e: [], ready: function (f) { t._e.push(f) } });
            }(document, "script", "twitter-wjs"));
            </script>
		</div>
		<div id="header_controls" class="header_block">
            <div id="login_controls">
                <h3><?=__('For observers')?></h3>
                <?if(!Auth::instance()->logged_in()):?>
                <?=Form::open('/users/login',array('method'=>'POST'))?>
            	    <input type="hidden" name="login_form_sent" value="1">
                    <table>
                        <tr><td class="label"><?=__('Login')?></td><td><input type="text" name="username" tabindex="1"></td> <td rowspan="2" valign="center">
                            <?=Form::image('login','login',array('src'=>'assets/images/login_btn.jpg','tabindex'=>3))?></td></tr>
                        <tr><td><?=__('Password')?></td><td><input type="password" name="password" tabindex="2"></td></tr>
                    </table>
                </form>
                <div id="reg_links">
                    <?=HTML::anchor('users/register',__('Register as observer'))?>
                    <?/*HTML::anchor('users/forgot',__('Forgot password'))*/?>
                </div>
                <?else:?>
            	    <h5>Добро пожаловать <?=Auth::instance()->get_user()->username?></h5>
                    <div><?=Html::anchor('users/logout',__('Logout'))?></div>
            	    <br><br>
                <?endif;?>
            </div>
            <!--div id="site_controls">
                <form method="POST">
                    <input type="text" name="search" value="Поиск по сайту"> <?=Form::button('search','Найти')?>
                </form>
                <span class="consultation">Консультации: <?=HTML::anchor('','skype',array('id'=>'skype_consult_link'))?> &nbsp;
                    <?=HTML::anchor('','icq',array('id'=>'icq_consult_link'))?>
                </span>
            </div-->
		</div>

		<div class="clear"></div>
		<nav>
			<ul id="topmenu" class="menu">
				<!--li><?=HTML::anchor("stats",'Главная')?></li-->
                <? if(Auth::instance()->logged_in()):?>
				<li class="accentuated"><?=HTML::anchor('votes',__('Add protocol'))?></li>
				<li class="accentuated"><?=HTML::anchor('violations',__('Note violation'))?></li>
                <? endif;?>
				<!--li><?=HTML::anchor("violations/video",'Видео нарушений')?></li-->
				<li><?=HTML::anchor("stats",'Статистика')?></li>
                <!--li><?=HTML::anchor("welcome/howitworks",__('How it works'))?></li-->
				<li><?=HTML::anchor("welcome/contacts",__('Contacts'))?></li>
				<li><?=HTML::anchor("welcome/details",__('About'))?></li>
                <li><?=HTML::anchor("welcome/forparties",__('For parties and candidates'))?></li>
				  <? /*
				  foreach($menu_links as $link){
					  echo '<li>'.Html::anchor($link['href'],$link['title']).'</li>';
				  }
				  ?>
				  <? if(Auth::instance()->logged_in()):?>
					  <li class="last"><?=Html::anchor('users/logout','Выйти')?></li>
				  <?else:?>
					<li class="last"><a href="#">Авторизация</a>
					  <ul>
						<li><?=Html::anchor('users/register','Зарегистрироваться')?></li>
						<li><?=Html::anchor('users/login','Войти')?></li>
					  </ul>
					</li>
				  <?endif; */?>
			</ul>
			
		</nav>
	</header>
	<div id="body">
        <div id="sideblock">
            <ul id="sidemenu" class="menu sf-menu sf-vertical">
                <?
                    function print_menu($menu){
                        if(is_array($menu)){
                            foreach($menu as $key=>$element){
                                if(is_array($element)){
                                    echo '<li>'.HTML::anchor($key,Arr::get($element,'title')).'<ul>';
                                    $children = Arr::get($element,'children',array());
                                    print_menu($children);
                                    echo '</ul></li>';
                                }
                                else{
                                    echo '<li>'.HTML::anchor($key,$element).'</li>';
                                }
                            }
                        }
                    }
                    print_menu($side_menu);
                ?>
            </ul>
            <?=HTML::image('assets/images/sidelogo.jpg')?>
        </div>

        <?=$content?>

	<div class="clear"></div>
	</div>
	<footer>
		<div id="footer_flag">
		</div>
		<div id="footer_info">
			<div id="footer_copyright">
				<p> Copyright  2014. &copy; <a href="mailto:uavibor@gmail.com">UAвыбор</a><br/> All rights reserved</p>
			</div>
			<div id="footer_details">
				<div>Email: <a href="mailto:uavibor@gmail.com">uavibor@gmail.com</a></div>
            <a class="twitter-follow-button"
              href="https://twitter.com/uavibor_com"
              data-show-count="false"
              data-lang="en">
            Follow @uavibor_com
            </a>

			</div>
		</div>
		<div class="clear"></div>
	</footer>




    
 <!-- footer ends -->
   <script type="text/javascript">
       $(document).ready(function() {
                 $('ul.sf-menu').superfish();
                 
                 var window_height = $(window).height();
                 var body_height = $('body').height();
                 if(body_height < window_height)
                 {
                    $('#main').height($('#main').height() + window_height - body_height - 10);
                 }
         });
   </script>
                                               
	</body>

</html>
