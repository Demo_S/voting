<!DOCTYPE html>
<html>
	<head>
		<title>
			<?=$title;?>
		</title>

		<?=$metas?>

		<?=$styles?>

		<?=$scripts?>
	</head>
<?flush()?>
<body>


<!-- content begins --> 
	    <div id="content">
		    <div class="menu">
                <?
                foreach($menu_links as $link){
                    echo '<div>'.Html::anchor($link['href'],$link['title'],array('class'=>'menulink')).'</div>';
                }
                ?>
		    </div>
			<div class="content ">
			<?=$content?>
			</div>

			
    	</div>




<!-- footer begins -->
	<footer>
		<div class="footer_content">
		 Copyright  2014. &copy; <a href="">A-group</a> All rights reserved<br />
		<a href="#">Privacy Policy</a> | <a href="#">Terms of Use</a>
		</div>
	</footer>
 <!-- footer ends -->
	</body>

</html>
