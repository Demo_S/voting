<div class="block column9">
<?php
    foreach($violations as $violation){
	var_dump($violation->as_array());
    }
?>
<div class="block">
    <div class="column column8">
	<?php if($okrug && isset($okrug['id']) && isset($okrug['details'])){echo '<h3>Округ '.$okrug['id'].': '.$okrug['details'].'</h3>';}?>
        <div id="violations">
            <h1>Графики</h1>
            <div id="violations_container">
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>



<script type="text/javascript">


      // Create the chart
$(function () {

  $(document).ready(function() {

      var colors = Highcharts.getOptions().colors,
          categories = ['Агитация', 'Вмешательство', 'Двойное голосование', 'Помеха наблюдателям', 'Нарушение процедуры','Нарушение тайны','Нарушение подсчета','Другое'],
          name = 'Типы нарушений',
          data = [{
                  y: 10,
                  color: colors[0],
                  drilldown: {
                      name: 'Округа',
                      categories: ['94 округ', '132 округ', '192 округ', '197 округ', '223 округ'],
                      data: [5, 2, 3,5,4],
                      color: colors[0]
                  }
              }, {
                  y: 8,
                  color: colors[1],
                  drilldown: {
                      name: 'Округа',
                      categories: ['94 округ', '132 округ', '192 округ', '197 округ', '223 округ'],
                      data: [4, 0, 4,0,0],
                      color: colors[1]
                  }
              }, {
                  y: 12,
                  color: colors[2],
                  drilldown: {
                      name: 'Округа',
                      categories: ['94 округ', '132 округ', '192 округ', '197 округ', '223 округ'],
                      data: [3, 6, 3, 5, 4],
                      color: colors[2]
                  }
              }, {
                  y: 15,
                  color: colors[3],
                  drilldown: {
                      name: 'Округа',
                      categories: ['94 округ', '132 округ', '192 округ', '197 округ', '223 округ'],
                      data: [3, 10, 2,4,2],
                      color: colors[3]
                  }
              }, {
                  y: 15,
                  color: colors[4],
                  drilldown: {
                      name: 'Округа',
                      categories: ['94 округ', '132 округ', '192 округ', '197 округ', '223 округ'],
                      data: [5, 5, 5,3,2],
                      color: colors[4]
                  }
              }, {
                  y: 10,
                  color: colors[5],
                  drilldown: {
                      name: 'Округа',
                      categories: ['94 округ', '132 округ', '192 округ', '197 округ', '223 округ'],
                      data: [4, 3, 3, 2, 1],
                      color: colors[5]
                  }
              }, {
                  y: 13,
                  color: colors[6],
                  drilldown: {
                      name: 'Округа',
                      categories: ['94 округ', '132 округ', '192 округ', '197 округ', '223 округ'],
                      data: [5, 2, 6, 6, 5],
                      color: colors[6]
                  }
              }, {
                  y: 20,
                  color: colors[7],
                  drilldown: {
                      name: 'Округа',
                      categories: ['94 округ', '132 округ', '192 округ', '197 округ', '223 округ'],
                      data: [5, 7, 8, 5, 4],
                      color: colors[7]
                  }
              }
		];

      function setChart(name, categories, data, color) {
	chart.xAxis[0].setCategories(categories, false);
	chart.series[0].remove(false);
	chart.addSeries({
		name: name,
		data: data,
		color: color || 'white'
	}, false);
	chart.redraw();
      }

      chart = new Highcharts.Chart({
          chart: {
              renderTo: 'violations_container',
              type: 'column'
          },
          title: {
              text: 'Нарушения'
          },
          subtitle: {
              text: 'Нажмите на колонку что б видеть распределение по округам.'
          },
          xAxis: {
              categories: categories
          },
          yAxis: {
              title: {
                  text: 'Общий процент от нарушений'
              }
          },
          plotOptions: {
              column: {
                  cursor: 'pointer',
                  point: {
                      events: {
                          click: function() {
                              var drilldown = this.drilldown;
                              if (drilldown) { // drill down
                                  setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                              } else { // restore
                                  setChart(name, categories, data);
                              }
                          }
                      }
                  },
                  dataLabels: {
                      enabled: true,
                      color: colors[0],
                      style: {
                          fontWeight: 'bold'
                      },
                      formatter: function() {
                          return this.y +'%';
                      }
                  }
              }
          },
          tooltip: {
              formatter: function() {
                  var point = this.point,
                      s = this.x +':<b>'+ this.y +'% нарушений</b><br/>';
                  if (point.drilldown) {
                      s += 'Нажмите для просмотра распределения по округам';
                  } else {
                      s += 'Нажмите для возврата';
                  }
                  return s;
              }
          },
          series: [{
              name: name,
              data: data,
              color: 'white'
          }],
          exporting: {
              enabled: false
          }
      });
  });

});

</script>
