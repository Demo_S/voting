<div class="block column9">

<div class="block">
    <div class="column column8">
	<?php if($okrug && isset($okrug['id']) && isset($okrug['details'])){ echo '<h3>Округ '.$okrug['id'].': '.$okrug['details'].'</h3>';}?>
        <div id="violations">
            <h1>Графики</h1>
            <div id="chart1">
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>



<script type="text/javascript">



$(function () {
  var chart;
  $(document).ready(function() {

      var colors = Highcharts.getOptions().colors,
          categories = ['94 округ', '132 округ', '192 округ', '197 округ', '223 округ'],
          name = 'Распределение голосов',
          data = [{
                  y: 30,
                  color: colors[0],
                  drilldown: {
                      name: 'Голоса',
                      categories: ['Иванов', 'Петров', 'Сидоров'],
                      data: [10, 20, 30],
                      color: colors[0]
                  }
              }, {
                  y: 20,
                  color: colors[1],
                  drilldown: {
                      name: 'Голоса',
                      categories: ['Гольдман', 'Кацман','Лоцман'],
                      data: [5,15, 25],
                      color: colors[1]
                  }
              }, {
                  y: 20,
                  color: colors[2],
                  drilldown: {
                      name: 'Голоса',
                      categories: ['Ткаченко', 'Шевченко','Кириченко'],
                      data: [11,9, 15],
                      color: colors[2]
                  }
              }, {
                  y: 15,
                  color: colors[3],
                  drilldown: {
                      name: 'Голоса',
                      categories: ['Сванидзе', 'Бабидзе','Гивидзе'],
                      data: [8,7, 15],
                      color: colors[3]
                  }
              }, {
                  y: 15,
                  color: colors[4],
                  drilldown: {
                      name: 'Голоса',
                      categories: ['Марио', 'Луцию','Игнассио'],
                      data: [ 11,4,14],
                      color: colors[4]
                  }
              }];


      // Build the data arrays
      var browserData = [];
      var versionsData = [];
      for (var i = 0; i < data.length; i++) {

          // add browser data
          browserData.push({
              name: categories[i],
              y: data[i].y,
              color: data[i].color
          });

          // add version data
          for (var j = 0; j < data[i].drilldown.data.length; j++) {
              var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
              versionsData.push({
                  name: data[i].drilldown.categories[j],
                  y: data[i].drilldown.data[j],
                  color: Highcharts.Color(data[i].color).brighten(brightness).get()
              });
          }
      }

      // Create the chart
      chart = new Highcharts.Chart({
          chart: {
              renderTo: 'chart1',
              type: 'pie'
          },
          title: {
              text: 'Распределение голосов'
          },
          yAxis: {
              title: {
                  text: 'Набранные голоса'
              }
          },
          plotOptions: {
              pie: {
                  shadow: false
              }
          },
          tooltip: {
      	    valueSuffix: '%'
          },
          series: [{
              name: 'Голоса',
              data: browserData,
              size: '60%',
              dataLabels: {
                  formatter: function() {
                      return this.y > 5 ? this.point.name : null;
                  },
                  color: 'white',
                  distance: -30
              }
          }, {
              name: 'Разбиение',
              data: versionsData,
              innerSize: '60%',
              dataLabels: {
                  formatter: function() {
                      // display only if larger than 1
                      return this.y > 1 ? '<b>'+ this.point.name +':</b> '+ this.y +'%'  : null;
                  }
              }
          }]
      });
  });


});

</script>
