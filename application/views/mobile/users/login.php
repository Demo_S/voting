<h2><?=__('Enter as registered user')?></h2>
<?php if (isset($message)): ?>
	<div class="error" style="color: red;">
		<?=$message?>
	</div>
<?php endif; ?>

<?=form::open('users/login')?>
<?=form::hidden('login_form_sent',1);?>
    <div data-role="fieldcontain">
        <fieldset data-role="controlgroup" data-mini="true">
            <label for="login_input">
                <?=__('Login')?>
            </label>
            <input name="username" id="login_input" placeholder="login" value="" type="text" />
        </fieldset>
    </div>
    <div data-role="fieldcontain">
        <fieldset data-role="controlgroup" data-mini="true">
            <label for="textinput2">
                <?=__('Password')?>
            </label>
            <input name="password" id="textinput2" placeholder="" value="" type="password" />
        </fieldset>
    </div>
    <input type="submit" data-theme="d" value="<?=__('Sign in')?>" data-mini="true" />
<?=form::close()?>

<br />
	<?=HTML::anchor('users/register',__('Register new user'))?> :: <?/*HTML::anchor('users/forgot',__('Forgot password'))*/?>