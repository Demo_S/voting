<h1>Edit <?=$username?>.</h1>
 
<?php 
if (isset($errors)){
	echo View::factory('error',array('error'=>$errors))->render();
}
 ?>
<div id="saveerror" class="error"></div>
<div class="user_fields">
<?=form::open('users/edit/'.$id,array('enctype'=>'multipart/form-data'))?>
<?=form::hidden('user_edit_sent',1);?>
<table>
<tr><td width="250px">
	Photo
</td><td>
	<?=is_file('upload/users/'.$id.'.jpg')?Html::image('upload/users/'.$id.'.jpg'):''?>
</td></tr>


<tr><td>Roles:</td><td>	Desired <?=Arr::get($avaiable_roles,$desired_role,'')?>
						<table class="inset_table">
						<?
							$roles = array_keys($roles);
							foreach($avaiable_roles as $roleid=>$name)
							{
								$checked = (in_array($roleid , $roles))?' checked ':'';
								echo '<tr><td><input type="checkbox" name="role['.$roleid.']" '.$checked.' value="1"></td><td>'.$name.'</td></tr>';
							}
						?>
						</table>

</td></tr>

<tr><td>
	<?=form::label('fullname', 'Full name:')?>
</td><td>
	<?=form::input('fullname',$fullname)?>
</td></tr>

<tr><td>
	<?=form::label('sex', 'Sex:')?>
</td><td>
	<?=form::select('sex',array(0=>'female',1=>'male'),$sex)?>
</td></tr>

<tr><td>
	<?=form::label('phone', 'Phone:')?>
</td><td>
	<?=form::input('phone',$phone)?>
</td></tr>

<tr class="client"><td>
	<?=form::label('company', 'Company:')?>
</td><td>
	<?=form::input('company',$company)?>
</td></tr>

<tr class="researcher_and_super"><td>
	<?=form::label('university', 'University:')?>
</td><td>
	<?=form::input('university',$university)?>
</td></tr>

<tr class="researcher_and_super"><td>
	<?=form::label('trainings', 'Trainings:')?>
</td><td>
	<?=form::input('trainings',$trainings)?>
</td></tr>

<tr class="researcher_and_super"><td>
	<?=form::label('degree','Degree: ')?>
</td><td>
	<?=form::select('degree_id', $degrees,$degree_id,array('class'=>'selectinput'))?>
	<?
		$SELECT_OTHER_VALUE = Kohana::config('misc.SELECT_OTHER_VALUE');
		$hidden = ($degree_id != $SELECT_OTHER_VALUE)?'hidden ':'';
		echo form::input('degree_other',$degree_other?$degree_other:'Type here',array('class'=>$hidden.'selectinput other'))
	?>
</td></tr>

<tr class="researcher_and_super"><td>
	<?=form::label('majorspecs[]','Major: (use &lt;ctrl&gt; key to select several)')?>
</td><td>
	<?=form::select('majorspecs[]', $majorspecs, $selected_majorspecs, array('multiple'=>'multiple','class'=>'selectinput'))?>
	<?
		$hidden = !in_array($SELECT_OTHER_VALUE,$selected_majorspecs)?'hidden ':'';
		echo form::input('majorspec_other',$majorspec_other?$majorspec_other:'Type here',array('class'=>$hidden.'selectinput other'))?>
</td></tr>

<tr class="researcher_and_super"><td>
	<?=form::label('researchcompetencies[]','Research Competency: (use &lt;ctrl&gt; key to select several)')?>
</td><td>
	<?=form::select('researchcompetencies[]', $researchcompetencies, $selected_researchcompetencies, array('multiple'=>'multiple','class'=>'selectinput'))?>
	<?
		$hidden = !in_array($SELECT_OTHER_VALUE,$selected_researchcompetencies)?'hidden ':'';
		echo form::input('researchcompetency_other',$researchcompetency_other?$researchcompetency_other:'Type here',array('class'=>$hidden.'selectinput other'))?>
</td></tr>

<tr class="researcher_and_super"><td>
	<?=form::label('spheresofinterest','Spheres of Interest: (use &lt;ctrl&gt; key to select several)')?>
</td><td>
	<?=form::select('spheresofinterest[]', $spheresofinterest, $selected_spheresofinterest, array('multiple'=>'multiple','class'=>'selectinput'))?>
	<?
		$hidden = !in_array($SELECT_OTHER_VALUE,$selected_spheresofinterest)?'hidden ':'';
		echo form::input('sphereofinterest_other',$sphereofinterest_other?$sphereofinterest_other:'Type here',array('class'=>$hidden.'selectinput other'))?>
</td></tr>
<tr><td colspan="2">
	<?=form::submit('save', 'Save')?>
</td></tr>
</table>
<?=form::close()?>
</div>