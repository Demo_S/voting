<h1><?=__('Please register')?></h1>
<h3><?= sprintf(__('You can %s if you have an account'),Html::anchor('users/login',__('sign in')))?></h3>
<?php
if (isset($errors) && $errors){
echo View::factory('errors',array('errors'=>$errors))->render();
}
?>
<div><?=__('Sorry, registration from mobile devices are nt available at the moment')?></div>
<? /*
<?=form::open('users/register',array('enctype' => 'multipart/from-data'))?>
<?=form::hidden('register_form_sent',1);?>

    <div data-role="fieldcontain">
        <fieldset data-role="controlgroup" data-mini="true">
            <label for="login_input">
                <?=__('Login')?>
            </label>
            <input name="username" id="login_input" placeholder="login" value="" type="text" />
        </fieldset>
    </div>
    <div data-role="fieldcontain">
        <fieldset data-role="controlgroup" data-mini="true">
            <label for="password_input">
                <?=__('Password')?>
            </label>
            <input name="password" id="password_input" placeholder="" value="" type="password" />
        </fieldset>
    </div>
    <div data-role="fieldcontain">
        <fieldset data-role="controlgroup" data-mini="true">
            <label for="password_repeat_input">
                <?=__('Repeat password')?>
            </label>
            <input name="password_confirm" id="password_repeat_input" placeholder="" value="" type="password" />
        </fieldset>
    </div>
    <div data-role="fieldcontain">
        <fieldset data-role="controlgroup" data-mini="true">
            <label for="email_input">
                Email
            </label>
            <input name="email" id="email_input" placeholder="<?=__('email')?>" value="" type="email" />
        </fieldset>
    </div>
    <div data-role="fieldcontain">
        <fieldset data-role="controlgroup" data-mini="true">
            <label for="fullname_input">
                <?=__('Full name')?>
            </label>
            <input name="fullname" id="fullname_input" placeholder="<?=__('Full name')?>" value="" type="text" />
        </fieldset>
    </div>
    <div data-role="fieldcontain">
        <fieldset data-role="controlgroup" data-mini="true">
            <label for="show_fio_checkbox">
                <?=__('Show your name')?>
            </label>
            <input name="show_fio" id="show_fio_checkbox" value="" type="checkbox" />
        </fieldset>
    </div>

    <div data-role="fieldcontain" data-mini="true">
    
        <label for="regions">
            <?=__('Region')?>
        </label>
        <?$regions[0] = '';?>
        <?=form::select('region_id',$regions, 0,array('id'=>'regions','data-theme'=>"d",'data-mini'=>"true"))?>
    </div>
    <div data-role="fieldcontain" >
        <label for="okrugs">
            <?=__('Okrug')?>
        </label>
        <?=form::select('okrug_id',array(), null,array('id'=>'okrugs','data-theme'=>"d", 'data-mini'=>"true"))?><!--Load via ajax-->
    </div>
    <div data-role="fieldcontain" data-mini="true">
        <label for="comission">
            <?=__('Comission')?>
        </label>
        <input name="comission" id="comission" placeholder="comission" value="" type="text" />
    </div>
    <input type="submit" data-theme="d" value="<?=__('Sign up')?>" data-mini="true" />
<?=form::close()?>

<script>
    $(function(){
        $('#regions').change(function(e){
            var region_id = $('#regions option:selected').val();
            $.get('/okrugs/okrugslist/'+region_id,null,function(res){
                $('#okrugs').html(res);
            },'html');
        });
        $('#okrugs').change(function(e){
            var okrug_id = $('#okrugs option:selected').val();
            $.get('/comissions/comissionlist/'+okrug_id,null,function(res){
                $('#comissions').html(res);
            },'html');
        });
    });
</script>
*/?>