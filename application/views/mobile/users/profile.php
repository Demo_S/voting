<div class="profile">
	

<div id="tabs" class="tabs">
    <ul>
	    <li><a href="#tabs-1"><?=__('Profile')?></a></li>
	    <li><a href="/users/player"><?=__('My players')?></a></li>
	    <!--li><a href="/logs/mylogs"><?=__('My logs')?></a></li>
	    <li><a href="/troops/index"><?=__('My troops')?></a></li-->
    </ul>
    <div id="tabs-1">
		<h3><?=__('Change password')?></h3>
		<?if(isset($error) && !empty($error)){
			echo '<div class="error">'.$error.'</div>';
		}?>
		<?=Form::open('users/profile',array('method'=>'POST'));?>
		<?=Form::hidden('action','change_password');?>
		<table>
			<tr><th><?=Form::label('old_password',__('Old password'))?></th><td><?=Form::password('old_password')?></td></tr>
			<tr><th><?=Form::label('new_password',__('New password'))?></th><td><?=Form::password('new_password')?></td></tr>
			<tr><th><?=Form::label('repeat_password',__('Repeat password'))?></th><td><?=Form::password('repeat_password')?></td></tr>
			<tr><td><?=Form::submit('save',__('Save'))?></td><td><input type="reset" name="Reset" value="<?=__('Reset')?>"></td></tr>
		</table>
		<?=Form::close()?>
	<!--User profile. change password, personal messages and so on-->
    </div>
</div>
<script>
    $(function() {
	    $( "#tabs" ).tabs({
		ajaxOptions: {
			error: function( xhr, status, index, anchor ) {
			    $( anchor.hash ).html(
				"Couldn't load this tab. We'll try to fix this as soon as possible. " +
				"If this wouldn't be a demo." );
			}
		}
	    });
    });
</script>
