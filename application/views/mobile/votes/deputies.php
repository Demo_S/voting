<div class="deputies input_list">
    <h2><?=__('Enter votes for deputies')?></h2>
    <p><?=__('Okrug:').$okrug_name?></p>
    <form method="POST" enctype="multipart/form-data">

        <?foreach($deputies as $deputy_id=>$deputy_name):?>
            <div data-role="fieldcontain">
                <fieldset data-role="controlgroup" data-mini="true">
                    <label for="deputy_<?=$deputy_id?>">
                        <?=$deputy_name?>
                    </label>
                    <input name="deputies[<?=$deputy_id?>]" id="deputy_<?=$deputy_id?>" value="<?=$deputies_votes[$deputy_id]?>" placeholder="votes" value="" type="text" />
                </fieldset>
            </div>
        <? endforeach;?>

        <div id="upload_protocol" data-role="fieldcontain">
       		<fieldset data-role="controlgroup" data-mini="true">
       		<label for="protocol_file"><?= __('Upload protocol') ?></label>
       		<input type="file" name="votingprotocol" id="protocol_file" placeholder="protocol" />
       		</fieldset>
       	</div>
        <input type="hidden" name="deputies_sent" value="1">
        <input type="submit" name="save_deputies" value="<?=__('Save votes')?>">
    </form>
    <div data-role="fieldcontain" >
        <div id="violationmedia">
            <?
            foreach($files as $id=>$path){
                $pathinfo = pathinfo($path);
                $thumb = misc::thumb_from_path($pathinfo);
                echo '<div class="uploaded_file_preview"><a href="#imgpopup_'.$id.'" data-rel="popup" ><img src="'.$thumb.'"</a>';
                echo '<div data-role="popup" id="#imgpopup_"'.$id.'><img src="'.$path.'"><div><a href="'.$id.'" class="deletelink">'.__('delete').'</a></div></div>';
            }
            ?>
        </div>
    </div>

</div>
<script type="text/javascript">
    $(function(){
        $('a.deletelink').click(function(e){
            e.preventDefault();
            if(confirm('<?=__('Are you sure you wan\'t to delete this file?')?>'))
            {
                var id = $(this).attr('href');
                var link = $(this);
                $.get('votes/delete_protocol/'+id,function(res){
                    if(res.status == 'FAIL'){
                        alert('<?=__('error deleting file: ')?>' + res.error);
                    }
                    else if(res.status== 'OK'){
                        link.parents('div.uploaded_file_preview').remove();
                    }
                })
            }
        });
    })
</script>