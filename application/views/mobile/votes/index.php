<div>
    <form method="POST">
    <div class="parties input_list">
        <h4></p><?=__('Enter votes for parties:')?></h4>
            <table>
                <? foreach($parties as $party_id=>$party_name): ?>
            	    <tr><td><label for="party_<?=$party_id?>"><?=$party_name?>:</label> </td><td><input type="text" name="parties[<?=$party_id?>]" id="party_<?=$party_id?>"></td><td> <?=__('votes')?></td></tr>
                <? endforeach;?>
            </table>
        </form>
    </div>
    <div class="deputies input_list">
        <h4></p><?=__('Enter votes for deputies:')?></h4>
        <form method="POST">
            <table>
                <? foreach($deputies as $deputy_id=>$deputy_name): ?>
            	    <tr><td><label for="deputy_<?=$deputy_id?>"><?=$deputy_name?>:</label> </td><td><input type="text" name="deputies[<?=$deputy_id?>]" id="deputy_<?=$deputy_id?>"></td><td> <?=__('votes')?></td></tr>
                <? endforeach;?>
            </table>
    </div>

<div id="upload_protocol" data-role="fieldcontain">
	<fieldset data-role="controlgroup" data-mini="true">
	<label for="protocol_file"><?= __('Upload protocol') ?></label>
	<input type="file" name="protocol" id="protocol_file" placeholder="protocol" />
	</fieldset>
</div>

    <input type="submit" name="submit" value="<?=__('Enter values')?>">
    </form>
</div>