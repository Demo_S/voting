<div class="parties input_list">
    <?if(count($errors)):?>
	<?foreach($errors as $error):?>
	    <div><?=$error?></div>
	<?endforeach?>
    <?endif?>
    <h2></p><?=__('Enter votes for parties:')?></h2>
    <form method="POST" enctype="multipart/form-data" action="/votes/parties" data-ajax="false">

            <?foreach($parties as $party_id=>$party_name):?>
                <div data-role="fieldcontain">
                    <fieldset data-role="controlgroup" data-mini="true">
                        <label for="party_<?=$party_id?>">
                            <?=$party_name?>
                        </label>
                        <input name="parties[<?=$party_id?>]" id="party_<?=$party_id?>" value="<?=Arr::get($parties_votes,$party_id)?>" placeholder="votes" value="" type="text" />
                    </fieldset>
                </div>
            <? endforeach;?>

        <div id="upload_protocol" data-role="fieldcontain">
       		<fieldset data-role="controlgroup" data-mini="true">
       		<label for="protocol_file"><?= __('Upload protocol') ?></label>
       		<input type="file" name="parties_protocol" id="protocol_file" placeholder="protocol" />
       		</fieldset>
       	</div>
        <input type="hidden" name="parties_sent" value="1">
        <input type="submit" name="save_parties" value="<?=__('Save votes')?>">
    </form>
    <div id="violationmedia">

        <?
        foreach($protocols as $id=>$path){
            $pathinfo = pathinfo($path);
            $thumb = misc::thumb_from_path($path);
            echo '<div class="uploaded_file_preview"><a href="#imgpopup_'.$id.'" data-rel="popup" >'.HTML::image($thumb).'</a></div>';
            echo '<div data-role="popup" id="#imgpopup_"'.$id.'>'.HTML::image($path).'<div><a href="'.$id.'" class="deletelink">'.__('delete').'</a></div></div>';
        }
        ?>
    </div>
</div>

</div>
<script type="text/javascript">
    $(function(){
        $('a.deletelink').click(function(e){
            e.preventDefault();
            if(confirm('<?=__('Are you sure you wan\'t to delete this file?')?>'))
            {
                var id = $(this).attr('href');
                var link = $(this);
                $.get('votes/delete_protocol/'+id,function(res){
                    if(res.status == 'FAIL'){
                        alert('<?=__('error deleting file: ')?>' + res.error);
                    }
                    else if(res.status== 'OK'){
                        link.parent('div.uploaded_file_preview').remove();
                    }
                })
            }
        });
    })
</script>