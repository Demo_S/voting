<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>
			<?=$title;?>
		</title>

		<?=$metas?>

		<?=$styles?>

		<?=$scripts?>
	</head>
<?flush()?>
<body>
        <!-- Home -->
        <div data-role="page" id="page1">
            <div data-theme="d" data-role="header" id="header">
                <h3>
                    <?=__('Voting control')?>
                </h3>
            </div>
            <div data-role="content" id="content" class="content">
                <?=$content?>
                <!--div data-role="collapsible-set" data-theme="d" data-content-theme="d">
                    <div data-role="collapsible" data-collapsed="false">
                        <h3>
                            Section Header
                        </h3>
						<div> content</div>
                    </div>
		
                </div-->
                <div>&nbsp;</div>
                <?
                    if($menu_links && is_array($menu_links))
                    {
                        echo '<ul data-role="listview" data-divider-theme="c" data-inset="false">
                                <li data-role="list-divider" role="heading">
                                    '.__('Your actions').'
                                </li>
                        ';
                        foreach($menu_links as $link){
                            echo '
                            <li data-theme="d">
                                <a href="'.$link['href'].'" data-transition="slide">
                                    '.$link['title'].'
                                </a>
                            </li>
                            '."\n";

                        }
                        echo '</ul>';
                    }
                ?>
            </div>
            <div data-theme="d" data-role="footer" id="footer">
				<div class="footer_content">
				 Copyright  2011. &copy; <a href="">A-group</a> All rights reserved<br />
				<a href="#">Privacy Policy</a> | <a href="#">Terms of Use</a>
				</div>
            </div>
        </div>
        <script>
            //App custom javascript
        </script>
 <!-- footer ends -->
	</body>

</html>
