<div id="mainpart">
	<?if($violation_posted){
		if($errors){
			echo View::factory('errors',array('errors'=>$errors))->render();
		}
		else{
			echo '<div>'.__('Data was saved successfully').'</div>';
		}
	}?>
    <h3><?=$violation_posted ? __('Inform about one more violation') : __('Inform about violation')?></h3>
    <form method="POST" enctype="multipart/form-data" data-ajax="false">
        <div data-role="fieldcontain">
            <fieldset data-role="controlgroup" data-mini="true">
                <label for="description">
                    <?=__('Description')?>
                </label>
                <textarea name="description" id="description" placeholder="description" value="" ></textarea>
            </fieldset>
        </div>
        <div data-role="fieldcontain" >
            <label for="violation_type">
                <?=__('Violation type')?>
            </label>
            <?=form::select('type',$violation_types, null,array('id'=>'violation_type','data-theme'=>"d", 'data-mini'=>"true"))?>
        </div>

	<div id="upload_violation_photo" data-role="fieldcontain">	
		<fieldset data-role="controlgroup" data-mini="true">
		<label for="violation_photo_file"><?= __('Violation photo') ?></label>
		<input type="file" name="violation_photo" id="violation_photo_file" placeholder="violation photo" />
		</fieldset>
	</div>
	<div id="upload_violation_video" data-role="fieldcontain">	
		<fieldset data-role="controlgroup" data-mini="true">
		<label for="violation_video_file"><?= __('Violation video') ?></label>
		<input type="file" name="violation_video" id="violation_video_file" placeholder="violation video" />
		</fieldset>
	</div>
	<input type="hidden" name="violation_sent" value="1">
	<input type="submit" name="violation_post" value="<?=__('Inform')?>">
	</form>
	<div><?=Html::anchor('/',__('Back to main'))?></div>
</div>