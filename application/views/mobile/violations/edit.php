<?php
/**
 * Created by JetBrains PhpStorm.  Author: Демо_С.
 * Date: 25.10.12  21:47
 * edit single violation. add/remove media files
 */
?>
<div>
    <h3><?=__('Edit violation')?></h3>
    <form method="POST" enctype="multipart/form-data">
        <div data-role="fieldcontain">
            <fieldset data-role="controlgroup" data-mini="true">
                <label for="description">
                    <?=__('Description')?>
                </label>
                <textarea name="description" id="description" placeholder="description" value="" ><?=$violation->description?></textarea>
            </fieldset>
        </div>
        <div data-role="fieldcontain" >
            <label for="violation_type">
                <?=__('Violation type')?>
            </label>
            <?=form::select('violation_type',$violation_types, $violation->violation_type,array('id'=>'violation_type','data-theme'=>"d", 'data-mini'=>"true"))?>
        </div>

        <div id="upload_violation_photo" data-role="fieldcontain">
       		<fieldset data-role="controlgroup" data-mini="true">
       		<label for="violation_photo_file"><?= __('Violation photo') ?></label>
       		<input type="file" name="violation_photo" id="violation_photo_file" placeholder="violation photo" />
       		</fieldset>
       	</div>

        <div id="upload_violation_video" data-role="fieldcontain">
       		<fieldset data-role="controlgroup" data-mini="true">
       		<label for="violation_video_file"><?= __('Violation video') ?></label>
       		<input type="file" name="violation_video" id="violation_video_file" placeholder="violation video" />
       		</fieldset>
       	</div>
         <input type="submit" name="violation_sent" value="Сообщить">
    </form>
    <div data-role="fieldcontain" >
        <div id="violationmedia">

            <?
            foreach($files as $id=>$path){
                $pathinfo = pathinfo($path);
                $thumb = misc::thumb_from_path($pathinfo);
                echo '<div class="uploaded_file_preview"><a href="#imgpopup_'.$id.'" data-rel="popup" ><img src="'.$thumb.'"</a>';
                echo '<div data-role="popup" id="#imgpopup_"'.$id.'><img src="'.$path.'"><div><a href="'.$id.'" class="deletelink">'.__('delete').'</a></div></div>';
            }
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('a.deletelink').click(function(e){
            e.preventDefault();
            if(confirm('<?=__('Are you sure you wan\'t to delete this file?')?>'))
            {
                var id = $(this).attr('href');
                var link = $(this);
                $.get('violations/deletefile/'+id,function(res){
                    if(res.status == 'FAIL'){
                        alert('<?=__('error deleting file')?>');
                    }
                    else if(res.status == 'OK'){
                        link.parents('div.uploaded_file_preview').remove();
                    }
                })
            }
        });
    })
</script>
