<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'digit'         => __(':field must be a digit'),
	'email'         => __(':field must be a email address'),
	'email_domain'  => __(':field must contain a valid email domain'),
	'equals'        => __(':field must equal :param2'),
	'min_length'    => __(':field must be at least :param2 characters long'),
	'max_length'    => __(':field must not exceed :param2 characters long'),
	'not_empty'     => __(':field must not be empty'),
	'numeric'       => __(':field must be numeric'),
	'phone'         => __(':field must be a phone number'),
	'range'         => __(':field must be within the range of :param2 to :param3'),
	'regex'         => __(':field does not match the required format'),
	'unique'	=> __(':field :param2 is already used'),
	'password_confirm' => __("password and confirmation doesn't match"),
);
