<?php defined('SYSPATH') or die('No direct script access.');

define('MAX_PHOTO_SIZE',5000000);
define('MAX_VIDEO_SIZE',35000000);
define('THUMB_WIDTH',150);
define('THUMB_HEIGHT',150);
class Misc 
{
	public static function ip2int($ipstr)
	{
		$ips = explode(',',$ipstr);
		$localip = 0;
		foreach($ips as $ipstr)
		{
			$ip_arr = explode('.',trim($ipstr));
			$ip = 0;
			for($i = 0; $i < count($ip_arr); $i++)
			{
			    $ip *= 256;
			    $ip += (int)$ip_arr[$i];
			}
			if(($ip_arr[0] == '127') || ($ip_arr[0] == '192')){
			    $localip = $ip;
			}
			else{
			    return $ip;
			}
		    
		}
		return $localip;
	}
	public static function strip_last_colon($str)
	{
	    $len = strlen($str);
	    if($str[$len-1] == ':')
	    {
		return substr($str, 0, $len-1);
	    }
	    else
	    return $str;
	}
    public static function thumb_from_path($pathname)
    {
        $p = pathinfo($pathname);
        return $p['dirname'].'/th_'.$p['filename'].'.jpg';
    }
    public static function upload_file($html_filename,$size,$is_image,$path)
    {
        if(isset($_FILES[$html_filename]) && is_uploaded_file($_FILES[$html_filename]['tmp_name'])){
             $validate = Validation::factory($_FILES);
             $validate->rules($html_filename,
                 array(array('Upload::valid'),
                       array('Upload::not_empty'),
                       array('Upload::size', array(':value',$size)))
             );
            if($is_image)
            {
                $validate->rule($html_filename,array('Upload','image'));
            }
             if($validate->check()){
                $name = time() . $_FILES[$html_filename]['name'];
                $directory = "assets/$path/";
                if (  is_dir($directory) AND is_writable(realpath($directory)))
                {
            		$saved_path = $directory.DIRECTORY_SEPARATOR.$name;
            		if(move_uploaded_file($_FILES[$html_filename]['tmp_name'],$saved_path))
            		{

            			$image = Image::factory($saved_path);
            			$image->resize(THUMB_WIDTH,THUMB_HEIGHT,Image::INVERSE);
            			$image->crop(THUMB_WIDTH,THUMB_HEIGHT);
            			$thumb_path = self::thumb_from_path($saved_path);
            			$image->save($thumb_path);
				
            			 return $saved_path;
            		}
                 }
             }
        }
        return false;
    }
    public static function format_validation_error_if_any($validation_array, $field)
    {
	if(isset($validation_array[$field])){echo '<span class="error">'.$validation_array[$field].'</span>';}
    }
}
