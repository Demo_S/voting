<?php defined('SYSPATH') or die('No direct script access.');

class Kohana_Exception extends Kohana_Kohana_Exception {
    
    public static function text(Exception $e)
    {
	return sprintf('user %d, %s [ %s ]: %s ~ %s [ %d ]'."\n%s\n-----------==-----------\n",
	    Session::instance()->get('user_id'),
	    get_class($e), $e->getCode(), strip_tags($e->getMessage()), Debug::path($e->getFile()), $e->getLine(),var_export($e->getTrace(),true));
    }
    
    public static function handler( Exception $exception ) {

      if ( Kohana::$environment === Kohana::DEVELOPMENT ) {

        parent::handler( $exception );

      } else {
    if(is_object(Kohana::$log)){
	Kohana::$log->add( Log::ERROR, Kohana_Exception::text( $exception ) );
	Kohana::$log->write();
    }
        $errorCode = $exception->getCode();

        try {

          echo
            Request::factory( "error/index/".$errorCode )
              ->execute()
              ->send_headers()
              ->body()
	    ;

        } catch ( Exception $exception ) {echo 'Error while handling exception';}

      }
    }
}
