<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Stats extends Controller_Deftemplate { //statistics about voting

	public function before()
	{
	    parent::before();
        //check users login
	    $this->title('Stats');
        $this->add_script('charts/highcharts.js');
        $this->add_style('map.css');
        $regions = ORM::factory('region')->select('id','name')->where('enabled','=',1)->find_all()->as_array('id','name');
        $region_menu = array();
        foreach($regions as $id=>$region_name){
            $okrugs = ORM::factory('okrug')->select('number','name')->where('region_id','=',$id)->order_by('number')->find_all();
            $okrug_menu = array();
            foreach($okrugs as $okrug){
		/*$iks = ORM::factory('comission')->select('id')->where('okrug_id','=',$okrug->id)->find_all();
		$iks_menu = array();
		foreach($iks as $ik){
            	    $iks_menu['stats/bycomission/'.$ik->id] = __('IK').' '.$ik->id;
		}
                $okrug_menu[] = array(
                    'title' => $okrug->name,
                    'children' => $iks_menu
                );*/
		$okrug_menu['stats/byokrug/'.$okrug->id] = $okrug->name;
            }
            $region_menu['stats/byregion/'.$id] = array(
                'title' => $region_name,
                'children' => $okrug_menu
            );
        }
        $this->side_menu = array('stats/index'=>__('Global stats'),
                                'stats/byregion'=>array(
                                        'title'=>__('By region/okrug/comission'),
                                        'children'=>$region_menu
                                        ),
                                );
        $this->add_admin_sidemenu_items();
	}
    public function action_index()
    {
        $parties = ORM::factory('party')->select('name')->order_by('id')->find_all()->as_array(null,'name');
    	$this->content(View::factory('stats/index',array('parties'=>$parties)));
    }
    public function action_violations()
    {
	    $scale = $this->request->param('id');
        switch($scale)
        {
            case 'global':
                $this->violations_global();
                break;
            case 'regions':
                $this->violations_regions();
                break;
            case 'comissions':
                $this->violations_comissions();
                break;
            default:
                $this->violations_global();
                break;
        }
    }

    /**
     * нарушения по украине
     */
    protected function violations_global()
    {
        $violations = ORM::factory('violations')->count_all();
        $this->content(View::factory('violations/global',array('violations'=>$violations)));
    }

    /**
     * нарушения по областям
     */
    protected function violations_regions()
    {
        $violations = ORM::factory('violations')->select(array(DB::expr('count(*)'),'total'))->select('regionid')->group_by('regionid')->find();
        $this->content(View::factory('violations/global',array('violations'=>$violations)));
    }

    /**
     * нарушения по комиссиям
     */
    protected function violations_comissions()
    {
        $region_id = $this->request->param('id');
        $violations = ORM::factory('violations')->select(array(DB::expr('count(*)'),'total'))->select('comissionid')
            ->where('regionid','=',(int)$region_id)
            ->group_by('comissionid')->find();
        $this->content(View::factory('violations/global',array('violations'=>$violations)));
    }

    /**
     * Голосование по украине
     */
    public function action_votesglobal()
    {

    }
    /**
     * голосование по области
     * @param $regionid - id области
     */
    public function action_votesregion($regionid)
    {

    }

    /**
     * голосование по определенной комиссии
     * @param $comissionid - id комиссии
     */
    public function action_votescomission($comissionid)
    {

    }

    public function action_global(){
        $this->content(View::factory('error',array('error'=>'Извините, данная страница еще не реализована')));
    }
    public function action_violationsbyokrug(){
        $okrug_id = $this->request->param('id');
        $okrug = ORM::factory('okrug',$okrug_id);
        if($okrug){
	    $okrug = $okrug->as_array();
        }
        else{
	    $okrug = false;
        }
        $violations = ORM::factory('violation')->select(DB::expr('count(*) as violations_count'))->where('oik_id','=',$okrug_id)->group_by('type')->find_all();
        $this->content(View::factory('stats/violationsbyokrug',array('violations'=>$violations,'okrug'=>$okrug)));
    }
    public function action_votesbyokrug(){
        $okrug_id = $this->request->param('id');
        $okrug = ORM::factory('okrug',$okrug_id);
        if($okrug){
	    $okrug = $okrug->as_array();
        }
        else{
	    $okrug = false;
        }
        $votes = ORM::factory('v2oik_DeputiesVotes')->where('oik_id','=',$okrug_id)->find_all();
        $this->content(View::factory('stats/votesbyokrug',array('votes'=>$votes,'okrug'=>$okrug)));
    }
 }
