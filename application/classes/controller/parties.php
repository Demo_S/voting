<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Parties extends Controller_Admin {
    protected $security_roles = array('admin');
	protected $orm_name = 'party';
	public function before()
	{
	    parent::before();
        //check users login
	    $this->title('Parties');
	}
 }
