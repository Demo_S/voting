<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Regions extends Controller_Admin {

    protected $security_roles = array('admin');
	protected $orm_name = 'region';
	public function before()
	{
	    parent::before();
        //check users login
	    $this->title('Regions');
	}
 }
