<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Deftemplate {

	public function action_index()
	{ 
    	    if($this->is_mobile){
            if(Auth::instance()->logged_in()){
                $this->menu_links[] = array('href'=>'/users/logout','title'=>'Выйти из системы');
            }
            else{
                $this->menu_links = array(
                                array('href'=>'/users/login','title'=>'Войти в систему'),
                                array('href'=>'/users/register','title'=>'Зарегистрироваться'),
                                array('href'=>'/welcome/details','title'=>'О проекте'),
                                array('href'=>'/stats','title'=>'Посмотреть результаты'),
                            );
        	}
    	    }
        $this->content(new View('welcome/index'));
	}
    public function action_details()
    {
        $this->add_style('div-bootstrap.css');
        $this->content(new View('welcome/details'));
    }
    public function action_forparties()
    {
        $this->add_style('div-bootstrap.css');
        $this->content(new View('welcome/forparties'));
    }
    public function action_social()
    {
        $this->add_style('div-bootstrap.css');
        $this->content(new View('welcome/social'));
    }
	public function action_contacts()
	{
		$this->content(new View('welcome/contacts'));
	}
	public function action_test()
	{
	    if(isset($_REQUEST['code'])){
		$code = $_REQUEST['code'];
		$data = file_get_contents('https://oauth.vk.com/access_token?client_id=3730319&client_secret=cmB6kqdHm2L1MQdJrav8&code='.$code.'&redirect_uri='.urlencode('voting.t4log.com/welcome/test'));
		var_dump($data);
	    }
	    else{
		print "no code";
	    }
	    die();
	}

} // End Welcome
