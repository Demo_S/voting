<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Comissions extends Controller_Admin {

    protected $security_roles = array('admin');
	protected $orm_name = 'comission';
	

	public function before()
	{
	    parent::before();
        //check users login
	    $this->title('Comissions');
	}
    public function action_index()//id, name, descr, link
    {
	$okrug_id = $this->request->param('id');
	$items_orm = ORM::factory('comission')->order_by('okrug_id')->order_by($this->order_column);
	if($okrug_id){
	    $items_orm->where('okrug_id','=',$okrug_id);
	}
	$items = $items_orm->find_all();

        $okrugs = ORM::factory('okrug')->order_by('region_id')->order_by('name')->select('id','name')->find_all()->as_array('id','name');
        $this->content(View::factory($this->request->controller().'/index',array('items'=>$items,'okrugs'=>$okrugs)));
    }
 }
