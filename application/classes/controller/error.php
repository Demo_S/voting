<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Error extends Controller {

    public function action_index()
    {
	$errno = $this->request->param('id');
	$this->response->body(View::factory('error',array('error'=>$errno))->render());
    }
}