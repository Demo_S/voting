<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Violations extends Controller_Deftemplate {

    private $violation_types = array(1=>'Агитация',2=>'Вмешательство',3=>'Повтроное голосование',4=>'Помеха наблюдателям',5=>'Нарушение процедуры',6=>'Нарушение тайны',7=>'Нарушение подсчета',8=>'Другое');
	const MEDIA_TYPE_PHOTO = 1;
	const MEDIA_TYPE_VIDEO = 2;
	public function before()
	{
	    parent::before();
        if($this->is_mobile){
               $this->menu_links = array(
                                           array('href'=>'/violations/index','title'=>'Зафиксировать нарушение'),
                                           array('href'=>'/violations/list','title'=>'Список введенных Вами нарушений'),
                                           array('href'=>'/votes/parties','title'=>'Зафиксировать результаты голосования'),
                                           array('href'=>'/welcome','title'=>'На главную'),
                                       );
               }
               if(!Auth::instance()->logged_in()){
           	    $this->request->redirect('/users/loginrequired');
               }
	    $this->title('Uploads');

	}
	public function action_index()
	{	
		$violation_posted = Arr::get($_REQUEST,'violation_sent') == 1;
		$errors = false;
        if($violation_posted)
        {
            $violation = ORM::factory('violation');
            $violation->values($_REQUEST);//type, description
            $violation->ts = time();
            $user = Auth::instance()->get_user();
            $violation->user_id = $user->id;
            $violation->oik_id = $user->okrug_id;
            $violation->ik_id = $user->comission_id;
            $errors = array();
            $success = false;
            try{
                $violation->save();
                $success = true;
            }
            catch(ORM_Validation_Exception $e)
            {
                $errors[] = $e->errors('model');
            }
            if($success){
                $res = misc::upload_file('violation_photo',MAX_PHOTO_SIZE,true,'violations/photos');
                if($res){
            	    $e = $this->savemedia($res,$violation, self::MEDIA_TYPE_PHOTO);
            	    if(is_array($e) && count($e)){
                	$errors[] = $e;
                    }
                }
                $res = misc::upload_file('violation_video',MAX_VIDEO_SIZE,false,'violations/videos');
                if($res){
            	    $e = $this->savemedia($res,$violation, self::MEDIA_TYPE_VIDEO);
            	    if(is_array($e) && count($e)){
                	$errors[] = $e;
                    }
                }
            }

        }
	    $this->content(View::factory('violations/index',array('violation_types'=>$this->violation_types,'errors'=>$errors,'violation_posted'=>$violation_posted)));
	}
    public function action_list()
    {
        $violations = ORM::factory('violation')->where('user_id','=',Auth::instance()->get_user()->id)->find_all();
        $this->content(View::factory('violations/list',array('violations'=>$violations,'violation_types'=>$this->violation_types)));
    }
    public function action_view()
    {
        $violation_id = $this->request->param('id');
        $violation = ORM::factory('violation',$violation_id);
        if($violation->loaded())
        {
            $violation->violation_type_name = self::$violation_types[$violation->violation_type];
            $files = ORM::factory('violation_media')->where('violation_id','=',$violation_id)->select('id','path')->find_all('id','path')->as_array('id','path');
        }
        else{
            $files = array();
        }
        $this->add_style('prettyPhoto.css');
        $this->add_script('jquery.prettyPhoto.js');
        $this->content(View::factory('violations/view',array('violation'=>$violation,'files'=>$files)));
    }
    public function action_edit()
    {
        $violation_id = $this->request->param('id');
        $violation = ORM::factory('violation',$violation_id);
        if($violation->loaded())
        {
            $user_id = Auth::instance()->get_user()->id;
            if(Arr::get($_REQUEST,'violation_sent') == 1){
                $violation->values($_REQUEST);
                try{
                    $violation->user_id = $user_id;
                    $violation->save();
                }
                catch(ORM_Validation_Exception $e){
                    $errors[] = $e->errors('model');
                }
            }
            if(count($_FILES)){//add more files
                if(isset($_FILES['violation_photo'])){
                    $path = misc::upload_file('violation_photo',MAX_PHOTO_SIZE,true,'violations/photos');
                    $e = $this->savemedia($path,$violation, self::MEDIA_TYPE_PHOTO);
                    if(is_array($e) && count($e)){
                	$errors[] = $e;
                    }
                }
                if(isset($_FILES['violation_video'])){
                    $path = misc::upload_file('violation_video',MAX_VIDEO_SIZE,false,'violations/videos');
                    $e = $this->savemedia($path,$violation, self::MEDIA_TYPE_VIDEO);
                    if(is_array($e) && count($e)){
                	$errors[] = $e;
                    }
                }
            }
            $files = ORM::factory('violation_media')->where('violation_id','=',$violation_id)->select('id','path')->find_all('id','path')->as_array('id','path');
        }
        else{
            $files = array();
        }
        if(!$this->is_mobile)
        {
            $this->add_style('prettyPhoto.css');
            $this->add_script('jquery.prettyPhoto.js');
        }
        $this->content(View::factory('violations/edit',array('violation'=>$violation,'files'=>$files,'violations_types'=>self::$violation_types,'errors'=>$errors)));

    }
    public function action_deletefile()
    {
        $this->auto_render = false;
        $this->response->headers('Content-type','text/json');
        $violationmedia = ORM::factory('violationmedia',$this->request->param('id'));
        if($violationmedia->loaded()){
            unlink($violationmedia->path);
            $violationmedia->delete();
            $res = 'OK';
        }
        else{
            $res = 'FAIL';
        }
        $this->response->body(json_encode(array('status'=>$res)));
    }
    private function savemedia($path,$violation, $type)
    {
        $violationmedia = ORM::factory('violationmedia');
        $violationmedia->path = $path;
        $violationmedia->violation_id = $violation->id;
        $violationmedia->user_id = $violation->user_id;
        $violationmedia->oik_id = $violation->oik_id;
        $violationmedia->ik_id = $violation->ik_id;
        $violationmedia->ts = time();
	$violationmedia->type = $type;
        try{
            $violationmedia->save();
            return true;
        }
        catch(ORM_Validation_Exception $e)
        {
            return $e->errors('model');
        }
    }

 } // End Uploads
