<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Deftemplate extends Controller_Exttemplate 
{
	protected $session_id = 0;
	protected $user_returned = true;
	protected $cache = null;
    protected $secure_actions = array();
    protected $security_roles = array();

	public function __construct(Request $request, Response $response)
	{
		parent::__construct($request, $response);
		$user = Auth::instance()->get_user();//black magic, just hack, for autologin to happen before anything other. cause autologin tries to set cookie, and it should be done before any headers sent
		$this->cache = Cache::instance();

	}
	public function before()
	{
		parent::before();
		$this->meta_equiv('Content-Type','text/html; charset=utf8');

		if($this->is_mobile) {
			//$this->add_style('https://ajax.aspnetcdn.com/ajax/jquery.mobile/1.1.1/jquery.mobile-1.1.1.min.css');
            $this->add_script('https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js');
			$this->add_style('http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css');
			$this->add_style('my.css');
            $this->add_style('styles.css');//not sure about this
			$this->add_script('http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js');
			//$this->add_script('https://ajax.aspnetcdn.com/ajax/jquery.mobile/1.2.0/jquery.mobile-1.2.0.min.js');
			$this->add_script('my.js');
		}
		else {
            $this->add_style('styles3.css');
	    $this->add_style('superfish/superfish.css');
	    $this->add_style('superfish/superfish-vertical.css');
	    
            $this->add_script('https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js');
            $this->add_script('modernizr-1.5.min.js');
            $this->add_script('jquery.easing-sooper.js');
            $this->add_script('superfish/superfish.min.js');
            $this->add_script('superfish/hoverIntent.js');
            //$this->add_script('jquery.kwicks-1.5.1.js');

			$this->add_style('http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css');
			$this->add_script('http://code.jquery.com/ui/1.9.0/jquery-ui.js');		
		}
		if(!$this->session_id = Cookie::get('voting_session')) //cookie is absent
		{
			$this->session_id = $this->generateSessionKey();
			$this->user_returned = false;
			Session::instance()->set('user_id',$this->session_id);
		}
		Cookie::set('voting_session',$this->session_id,3600*24*10);

        $this->menu_links = array(
                                  array('href'=>'/welcome','title'=>'Главная'),
                                  array('href'=>'/votes','title'=>'Добавить протокол'),
                                  array('href'=>'/violations','title'=>'Зафиксировать нарушение'),

                                  array('href'=>'/welcome/details','title'=>'О проекте'),
                                    array('href'=>'/welcome/contacts','title'=>'Связаться с нами'),
         );
        $this->side_menu = array('page/laws'=>__('Ukraine Laws'),
                                'page/complaints'=>__('For request makers'),
                                'page/last'=>__('Previous violations'),
                                'page/laweradvices'=>__('Lawyer advice'));
        $this->add_admin_sidemenu_items();
        //get action
        //check security_roles
        //check if action in $secure_actions
        //check all roles of current user to match
        //redirect if necessary
	$this->title = __('Elections control');

	}
    protected function add_admin_sidemenu_items()
    {
        if(Auth::instance()->logged_in('admin')){
            $this->side_menu['#1'] = array('title'=>__('Admin panel'),
                'children'=>array(
                    '/parties'=>__('Parties'),
                    '/regions'=>__('Regions'),
                    '/okrugs'=>__('Okrugs'),
                    '/deputies'=>__('Deputies'),
                ));
            $this->side_menu['#2'] = ['title'=>__('Party admin'),
                'children'=>[
                    '/users/partyobservers'=>__('Manage party observers')
                ]];
        }
        if(Auth::instance()->logged_in('party_admin')){

        }

    }
	public function after()
	{
	    if($this->auto_render)
	    {
	    }
	    parent::after();
	}
    protected function generateSessionKey()
    {
        $ip = Misc::ip2int(Request::$client_ip);
        mt_srand(time());
        $randvalue = mt_rand(100000000,429496729);
        return $ip . $randvalue;

    }
	protected function getAndCache($valuename,$default)
	{
	    $res = $this->cache->get($valuename,false);
	    if($res === false)
	    {
		$res = $default();
		$this->cache->set($valuename,$res);
	    }
	    return $res;
	}
}
