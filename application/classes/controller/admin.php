<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin extends Controller_Deftemplate {
    protected $security_roles = array('admin');
	protected $orm_name;
	protected $order_column = 'name';
	public function before()
	{
	    parent::before();
        $this->add_script('admin.js');
        if(!Auth::instance()->logged_in('admin')){
            $this->request->redirect('/');
        }
	}
    /**
     *  для амдинистрирования - редактирование списка партий (для всей украины)
     */
    public function action_index()//id, name, descr, link
    {		
		$items = ($this->orm_name) ? ORM::factory($this->orm_name)->order_by($this->order_column)->find_all() : array();
        $this->content(View::factory($this->request->controller().'/index',array('items'=>$items)));
    }
    public function action_delete()
    {
        $id = $this->request->param('id');
        $item = ORM::factory($this->orm_name,$id);
        if($item->loaded()){
            $item->delete();
            $status = 'OK';
        }
        else {
            $status = 'FAIL';
        }
        $this->content(array('status'=>$status),true);
    }
    public function action_save()//save/add
    {
        $id = $this->request->param('id');
        if($id){
            $orm = ORM::factory($this->orm_name,$id);
        }
        else{
            $orm = ORM::factory($this->orm_name);
        }
        $orm->values($_REQUEST);
        $saved_id = 0;
        try{
            $orm->save();
            $saved_id = $orm->id;
            $error = false;
            $status = 'OK';
        }
        catch(ORM_Validation_Exception $e)
        {
            $error = $e->errors('models');
            $status = 'FAIL';
        }
        $this->content(array('status'=>$status,'error'=>$error,'id'=>$saved_id),true);
    }

    private function action_parse()
    {
        $case = $this->request->param('id');
        if($case == 1)
        {
            $data = str_replace("\n",'',file_get_contents('assets/data/mazhor1.html'));
            $data = iconv('cp1251','utf8',$data);
            $matches = array();
            if(preg_match_all('|<tr>.*?</tr>|',$data,$matches))
            {
        	$matches = $matches[0];
                $i = 0;
                $region_name = '';
                $region_okrugs = 0;
                $region_id = 0;
                $current_okrug = 0;
                $matches_inner = array();
                while($i < count($matches))
                {
                    if($current_okrug == 0)
            	    {
                        if(preg_match('|<td .*rowspan="(.*?)" ?>(.*?)</td>|',$matches[$i],$matches_inner)){

                            $region_okrugs = $matches_inner[1];
                            $region_name = $matches_inner[2];
                            $region = ORM::factory('region');
                            $region->name = $region_name;
                            try{
                                $region->save();
                                $region_id = $region->id;
                            }
                            catch(ORM_Validation_Exception $e)
                            {
                                var_dump($e->errors());
                            }
                        }
                        else{
                            var_dump('error, current_okrug=0, no rowspan');
                        }
                    }
                    if(preg_match_all('|<td >(.*?)</td>|',$matches[$i],$matches_inner)){
                	$matches_inner = $matches_inner[1];
                        $number = trim($matches_inner[0]);
                        $okrug = ORM::factory('okrug');
                        $okrug->number = trim($number);
                        $okrug->region_id = $region_id;
                        $okrug->details = trim($matches_inner[1]);
                        $okrug_id = 0;
                        try{
                            $okrug->save();
                            $okrug_id = $okrug->id;
                        }
                        catch(ORM_Validation_Exception $e){
                            var_dump($e->errors());
                        }
                        
                	$current_okrug++; 
                	if($current_okrug >= $region_okrugs)
                	    $current_okrug = 0;
                        
                        for($j = 2; $j < count($matches_inner); $j++){
                    	    $deputy_name = trim($matches_inner[$j]);
                    	    if(strlen($deputy_name)){
                    	    if($j == 9){
                    		$deputies = explode('<br />',$deputy_name);
                    		foreach($deputies as $deputy){
                            $orm = ORM::factory('deputy');
                            $orm->name = trim($deputy);
                            $orm->okrug_id = $okrug_id;
                            $orm->party_id = $j-1;
                            try{
                                $orm->save();
                            }
                            catch(ORM_Validation_Exception $e)
                            {
                                var_dump($e->errors());
                            }
                            }
                    	    }
                    	    else{
                    	    if($j == 3)//svoboda ebannaya
                    	    {
                    		$words = explode(' ',$deputy_name);
                    		$total = count($words);
                    		if($total > 3){
                    		    $deputy_name = $words[$total-2].' '.$words[$total-1];
                    		}
                    	    }
                            $orm = ORM::factory('deputy');
                            $orm->name = $deputy_name;
                            $orm->okrug_id = $okrug_id;
                            $orm->party_id = $j-1;
                            try{
                                $orm->save();
                            }
                            catch(ORM_Validation_Exception $e)
                            {
                                var_dump($e->errors());
                            }
                            }
                            }
                        }//for
                    }
                    $i++;
                }//while
            }//if preg

        }//if case
        elseif($case == 2)
        {
            $data = file_get_contents('assets/data/kpu');
            $lines = explode("\n",$data);
            $i = 0;
            while($i < count($lines))
            {
                $number = (int)trim($lines[$i]);
                if($number -1 != (int)$i/2) {var_dump($i,$number,$lines[$i]);echo '<br>';}
                $name = trim($lines[$i+1]);
                $orm = ORM::factory('deputy');
                $orm->okrug_id = $number;
                $orm->name = $name;
                $orm->party_id = 9;
                try{
                    $orm->save();
                }
                catch(ORM_Validation_Exception $e)
                {
                    var_dump($e->errors());
                }
                $i+=2;
            }
        }
    }

 }
