<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Deputies extends Controller_Admin {

    protected $security_roles = array('admin');
	protected $orm_name = 'deputy';

	public function before()
	{
	    parent::before();
        //check users login
	    $this->title('Deputies');
	}
    /**
      * список депутатов в мажоритарном округе
      */
     public function action_index()
     {
		$okrug_id = $this->request->param('id');
         $parties = ORM::factory('party')->select('id','name')->order_by('id')->find_all()->as_array('id','name');
	$parties[0] = 'Самовисуванець';
         if($okrug_id) {
             $deputies = ORM::factory('deputy')->where('okrug_id','=',(int)$okrug_id)->order_by('name')->find_all();
             $this->content(View::factory('deputies/okrug',array('items'=>$deputies,'okrug_id'=>$okrug_id,'parties'=>$parties)));
         }
         else {
             $deputies = ORM::factory('deputy')->order_by('okrug_id')->order_by('name')->find_all();
             $okrugs = ORM::factory('okrug')->order_by('id')->find_all()->as_array('id','name');
             $this->content(View::factory('deputies/index',array('items'=>$deputies,'okrugs'=>$okrugs,'parties'=>$parties)));

         }
     }
    public function action_grab()//deputies grab
    {
        $this->auto_render = false;
        require_once Kohana::find_file('vendor','phpquery/phpQuery','php');
        for($oik = 11; $oik <= 223; $oik++){
        $url = "http://91.203.63.34/web/vibori/$oik.html";
	/*
	$user_agent='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0 FirePHP/0.7.4';

        $options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
	    CURLOPT_HTTPHEADER     => ['Referrer: http://www.cvk.gov.ua/pls/vnd2014/wp032pt001f01=910.html'],
	    CURLOPT_COOKIE	   => 'synceduids=1; uuuuuuu=63e586a5uuuuuuu_63e586a5; dtimer_fname_1aname_12=1414345274; zwcutrb=1; zwcuvk=1; 1MdnkaJsnddum_sync_done=1; MdnkaJsnd_sync_done=1; WizTrSync=1; TDSTrSync3=1; facetz=%7B%22id%22%3A%22efd948b4-e224-486c-9e7c-91871bdd7d6d%22%2C%22categories%22%3A%7B%221%22%3A%7B%2211170347%22%3A%7B%22sources%22%3A%5B0%5D%2C%22extraInfo%22%3A%7B%7D%2C%22price%22%3A0%2C%22prob%22%3A1%7D%2C%2211170337%22%3A%7B%22sources%22%3A%5B0%5D%2C%22extraInfo%22%3A%7B%7D%2C%22price%22%3A0%2C%22prob%22%3A1%7D%2C%22111701607%22%3A%7B%22sources%22%3A%5B0%5D%2C%22extraInfo%22%3A%7B%7D%2C%22price%22%3A0%2C%22prob%22%3A1%7D%2C%22111702%22%3A%7B%22sources%22%3A%5B0%5D%2C%22extraInfo%22%3A%7B%7D%2C%22price%22%3A0%2C%22prob%22%3A1%7D%2C%222005%22%3A%7B%22sources%22%3A%5B11%5D%2C%22extraInfo%22%3A%7B%7D%2C%22price%22%3A0%2C%22prob%22%3A1%7D%2C%222004%22%3A%7B%22sources%22%3A%5B11%5D%2C%22extraInfo%22%3A%7B%7D%2C%22price%22%3A0%2C%22prob%22%3A1%7D%2C%221117081%22%3A%7B%22sources%22%3A%5B0%5D%2C%22extraInfo%22%3A%7B%7D%2C%22price%22%3A0%2C%22prob%22%3A1%7D%2C%22174103%22%3A%7B%22sources%22%3A%5B3%2C11%5D%2C%22extraInfo%22%3A%7B%7D%2C%22price%22%3A0%2C%22prob%22%3A1%7D%2C%2265%22%3A%7B%22sources%22%3A%5B11%5D%2C%22extraInfo%22%3A%7B%7D%2C%22price%22%3A20%2C%22prob%22%3A1%7D%2C%221117082%22%3A%7B%22sources%22%3A%5B0%5D%2C%22extraInfo%22%3A%7B%7D%2C%22price%22%3A0%2C%22prob%22%3A1%7D%7D%7D%2C%22synced%22%3Atrue%7D'
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $html = curl_exec( $ch );var_dump(count($html));
        $header  = curl_getinfo( $ch );
        curl_close( $ch );
	*/
	
        $html = file_get_contents($url);
        if($html){
            $pq = phpQuery::newDocumentHTML($html);
     		$table = $pq->find('table.t2:eq(1)');
            $first_row = $table->find('tr:first');
	    $rows = $first_row->nextAll();
            foreach($rows as $row){
		$row = pq($row);
                $td = $row->find('td:first a');
                $name = iconv('cp1251','utf8',str_replace('<br>',' ',$td->html()));
                $link = $td->attr('href');
                $td_party = $row->find('td:eq(2)');
                $party_name = $td_party->text();
                print '<div><a href="">'.$name.'</a> :: '.$party_name.'</div>';
                
                $deputy = ORM::factory('deputy');
                $deputy->okrug_id = $oik;
                $deputy->name = $name;
                $deputy->party_name = $party_name;

                try{
                    $deputy->save();
                }
                catch(ORM_Validation_Exception $e){
                    var_dump($e->errors('validation'));
                }
            }
        }
	}
        $this->response->body("SUCCESS");

    }
 }
