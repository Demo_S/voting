<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Exttemplate extends Controller 
{
	protected $config;
    protected $view_path = '';
	protected $view;

	protected $target	= '';

	protected $title;

	protected $metas	= array();

	protected $styles 	= array();

	protected $scripts 	= array();

	protected $content 	= array();

	protected $regions	= array();

	protected $auto_render = true;
	protected $is_mobile = false;
    protected $menu_links = array();
    protected $side_menu = array();
	/*
		Начальное заполнение шаблона
		- обнуление списков стилей и скриптов
		- обнуление шаблонных переменных
		- установка заголовка по умолчанию
		- установка начальной хлебной крошки
	*/
	public function before()
	{
		parent::before();

		
		$useragent=$_SERVER['HTTP_USER_AGENT'];

		$this->config = Kohana::$config->load('template');
		
		$this->is_mobile = preg_match('/android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|meego.+mobile|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
        View::$view_prefix = ($this->is_mobile) ? 'mobile/' : 'normal/';

		$this->view = new View;

		$this->view->set('title',  	'' );

		$this->view->set('metas',	'' );

		$this->view->set('styles', 	'' );
		$this->view->set('scripts',  	'' );
		

		$this->view->set('content', 	'' );

        $this->view->set('menu_links',	array() );
        $this->view->set('side_menu', array());
	}


	/*
		Устанавливает view, который является целью загрузки страницы
	*/
	public function target($path)
	{
		$this->target = $path;
	}


	/*
		Добавление meta-тега вида <meta name='name' content='content'>
	*/
	public function meta_name($title, $content)
	{
		$this->metas[] = array(
			'type'		=> 'name',
			'title'		=> $title,
			'content'	=> $content
		);
	}


	/*
		Добавление meta-тега вида <meta http-equiv='name' content='content'>
	*/
	public function meta_equiv($title, $content)
	{
		$this->metas[] = array(
			'type'		=> 'http-equiv',
			'title'		=> $title,
			'content'	=> $content
		);
	}


	/*
		Рендер meta-тегов для вывода на странице
	*/
	public function render_metas()
	{
		$data = '';

		foreach ($this->metas as $meta)
		{
			$data .= "<meta ".$meta['type']."='".$meta['title']."' content='".$meta['content']."'> \n";	
		}
		
		return $data;
	}



	/*
		Установка заголовка на страницу
	*/
	public function title($title)
	{
		$this->title = $title.' || '.$this->config->site_link;
	}


	/*
		Добавление нового HTML-блока в контент
		- по умолчанию в функцию передается объект класса View и во время добавления рендерится
		- если параметр "clear" установлен в "true", то считается, что передан уже готовый html
	*/
	public function content($html, $clear = false)
	{
		$this->content = ($clear == false ? $html->render() : $html);
	}


	/*
		Функция компиляции html-блоков контента в единый html код.
		- объединяет массив блоков в одну html строку и возвращает ее
	*/
	public function render_content()
	{
		if($this->request->is_ajax() && !$this->is_mobile)
		{
			return json_encode($this->content);
		}
		else{
			return $this->content;	
		}
	}



	/*
		Добавление ссылки на класс
	*/
	public function add_style($path)
	{
		if((strpos($path,'http://')!==false)||(strpos($path,'https://') !== false)){		    
			$this->styles[] = Html::style($path);
		}
		else{
			$this->styles[] = Html::style($this->config->media_dir."/css/".$path);
		}
	}


	/*
		Рендер ссылок на классы для вывода на странице
	*/
	private function render_styles()
	{
		$html = '';
		foreach ($this->styles as $style)
		{
			$html .= $style."\n";
		}
		return $html;
	}


	/*
		Добавление ссылки на скрипт
	*/
	public function add_script($path)
	{
		if((strpos($path,'http://')!==false)||(strpos($path,'https://') !== false)){
		    $this->scripts[] = "<script type='text/javascript' src='$path'></script>";			
		}
		else{
		    $this->scripts[] = Html::script($this->config->media_dir.'/js/'.$path);			
		}
	}


	/*
		Рендер ссылок на скрипты для вывода на странице
	*/
	private function render_scripts()
	{
		$html = '';
		foreach ($this->scripts as $script)
		{
			$html .= $script."\n";
		}
		return $html;
	}

	/*
		Функция рендера страницы
		- компилирует все переменные-массивы в единые html строки
		- привязывает все шаблонные переменные к главному view
		- производит рендер главного view и возвращает готовый html
	*/
	public function render()
	{
		$this->view->title 		= $this->title;

		$this->view->metas		= $this->render_metas();

		$this->view->styles 		= $this->render_styles();
		$this->view->scripts 		= $this->render_scripts();

		$this->view->content 		= $this->render_content();
        $this->view->menu_links     = $this->menu_links;
        $this->view->side_menu      = $this->side_menu;

		return $this->view->render($this->target);
	}


	/*
		Завершающая функция вывода шаблона
		- возвращает страницу пользователю в зависимости от запроса
	*/
	public function after()
	{
		if($this->auto_render)
		{
			if($this->target == ''){
				$this->target = $this->config->template_dir.'/'.$this->config->target;
			}

			if ( ! $this->request->is_initial() )	// Если запрос вызывается путем HMVC
			{
				$this->response->body( $this->render_content() );
			}
			elseif ($this->request->is_ajax() && !$this->is_mobile )  // Если запрос аяксом
			{
				$this->request->headers('Content-type','application/json; charset='.Kohana::$charset);
				$this->response->body( $this->render_content());
			}
			else  // Если обычный запрос
			{
				$this->response->body( $this->render() );
			}
		}
		parent::after();
	}

}
