<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Page extends Controller_Admin {
    protected $orm_name = 'page';
    protected $order_column='id';
	public function before()
	{
	    parent::before();
        //check users login
	    $this->title('Page');
	}
	public function action_view()
	{
		$pagename = $this->request->param('id');
		if(is_numeric($pagename)) {
			$page = ORM::factory('page',$pagename);
		}
		else {
			$page = ORM::factory('page')->where('innername','=',$pagename)->find();
		}
		if($page->loaded()){
			$this->title($page->title);
			$this->content(View::factory('page/view',array('content'=>$page->content)));
		}
		else{
			$this->content(View::factory('error',array('error'=>__('Sorry, but no such page. Please check if page address is written correctly'))));
		}
	}
	
 }
