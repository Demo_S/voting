<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Votes extends Controller_Deftemplate {

	public function before()
	{
	    parent::before();
        //check users login
	    $this->title('Votes');
	if($this->is_mobile){
        $this->menu_links = array(
                                    array('href'=>'/votes/parties','title'=>'Ввести результаты по партиям'),
                                    array('href'=>'/votes/deputies','title'=>'Ввести результаты по мажоритарным округам'),
                                    array('href'=>'/violations','title'=>'Зафиксировать нарушение'),
                                    array('href'=>'/welcome','title'=>'На главную'),
                                );
        }
        if(!Auth::instance()->logged_in()){
    	    $this->request->redirect('/users/loginrequired');
        }
	}
    /**
     * в котором он вводит как проголосовали закаждую партию по украине
     второе индивидуально для каждого округа где он вводит как проголосовали за
     мажоритарных кандидатов

     */
	public function action_index()
	{
        $this->request->redirect('votes/parties');
    }
    public function action_parties()
   	{
           $user = Auth::instance()->get_user();
           $user_id = $user->id;
           $errors = array();
           if(Arr::get($_REQUEST,'parties_sent') == 1)
           {
               $errors = $this->saveparties($user);
           }
           $parties = ORM::factory('party')->select('id','name')->find_all()->as_array('id','name');
           $parties_votes = ORM::factory('partyvote')->where('user_id','=',$user_id)->find_all()->as_array('party_id','votes');
           $protocols = ORM::factory('votingprotocol')->where('user_id','=',$user_id)->where('type','=',Model_Votingprotocol::$TYPE_PARTY_PROTOCOL)->find_all()->as_array('id','path');

           if(!$this->is_mobile)
           {
               $this->add_style('prettyPhoto.css');
               $this->add_script('jquery.prettyPhoto.js');
           }
           $this->content(View::factory('votes/parties',array('parties'=>$parties,'parties_votes'=>$parties_votes,'protocols'=>$protocols,'errors'=>$errors)));
    }
    public function action_deputies()
    {
        $user = Auth::instance()->get_user();
        $user_id = $user->id;
        $errors = array();
        if(Arr::get($_REQUEST,'deputies_sent') == 1)
        {
            $errors = $this->savedeputies($user);
        }
        $okrug_id = (int)Auth::instance()->get_user()->okrug_id;
        $okrug_orm = ORM::factory('okrug',$okrug_id);
        $okrug_name = $okrug_orm->loaded() ? $okrug_orm->name : '';
        $deputies = ORM::factory('deputy')->where('okrug_id','=',$okrug_id)->order_by('name')->find_all()->as_array('id','name');
        $deputies_votes = ORM::factory('deputyvote')->where('user_id','=',$user_id)->find_all()->as_array('deputy_id','votes');
        $protocols = ORM::factory('votingprotocol')->where('user_id','=',$user_id)->where('type','=',Model_Votingprotocol::$TYPE_PARTY_PROTOCOL)->find_all()->as_array('id','path');

        if(!$this->is_mobile)
        {
            $this->add_style('prettyPhoto.css');
            $this->add_script('jquery.prettyPhoto.js');
        }
        $this->content(View::factory('votes/deputies',array('deputies'=>$deputies,'okrug_name'=>$okrug_name,'deputies_votes'=>$deputies_votes,'errors'=>$errors,'protocols'=>$protocols)));
    }

    private  function saveparties($user)
    {
            $parties = Arr::get($_POST,'parties');//$parties - массив party_id->votes
            $errors = array();
            $now = time();
            foreach($parties as $party_id=>$votes)
            {
                $single_vote = ORM::factory('partyvote')->where('party_id','=',$party_id)->where('user_id','=',$user->id)->find();//single_vote->reset probably will be better
                if(!$single_vote->loaded()){
                    $single_vote = ORM::factory('partyvote');
                }
                $single_vote->user_id = $user->id;
                $single_vote->party_id = $party_id;
                $single_vote->ik_id = $user->comission_id;
                $single_vote->votes = $votes;
                $single_vote->ts = $now;
                try{
                    $single_vote->save();
                }
                catch(ORM_Validation_Exception $e)
                {
                    $errors[] = $e->getMessage();
                }
            }

            $res = Misc::upload_file('parties_protocol',MAX_PHOTO_SIZE,true,'protocols/parties');
            if($res){
                $protocol = ORM::factory('votingprotocol');
                $protocol->path = $res;
                $protocol->user_id = $user->id;
                $protocol->ik_id = $user->comission_id;
                $protocol->ts = time();
                $protocol->type = Model_Votingprotocol::$TYPE_PARTY_PROTOCOL;
                try{
                    $protocol->save();
                }
                catch(ORM_Validation_Exception $e){
                    $errors[] = 'Error saving protocol file '.$e->getMessage();
                }
            }
            else{
        	$errors[] = 'Error uploading protocol';
            }
            return $errors;
    }
    private  function savedeputies($user)
    {

            //как проголосовали за каждого мажоритарного кандидата
            $deputies = Arr::get($_POST,'deputies');//$deputies - массив deputy_id=>votes
            $now = time();
            foreach($deputies as $deputy_id=>$votes)
            {
                $single_vote = ORM::factory('deputyvote')->where('user_id','=',$user->id)->where('deputy_id','=',$deputy_id)->find();//single_vote->reset probably will be better
                if(!$single_vote->loaded()){
                    $single_vote = ORM::factory('deputyvote');
                }
                $single_vote->user_id = $user->id;
                $single_vote->party_id = deputy_id;
                $single_vote->ik_id = $user->comission_id;
                $single_vote->votes = $votes;
                $single_vote->ts = $now;
                try{
                    $single_vote->save();
                }
                catch(ORM_Validation_Exception $e)
                {
                    $errors[] = $e->getMessage();
                }

            }

            $res = Misc::upload_file('deputy_protocol',MAX_PHOTO_SIZE,true,'protocols/deputies');
            if($res){
                $protocol = ORM::factory('votingprotocol');
                $protocol->path = $res;
                $protocol->user_id = $user->id;
                $protocol->ik_id = $user->comission_id;
                $protocol->ts = time();
                $protocol->type = Model_Votingprotocol::$TYPE_DEPUTY_PROTOCOL;
                try{
                    $protocol->save();
                }
                catch(ORM_Validation_Exception $e){
                    $errors[] = 'Error saving protocol file '.$e->getMessage();
                }
            }

            return $errors;
    }
    public function action_delete_protocol()
    {
        $this->auto_render = false;
        $this->response->headers('Content-type','text/json');
        $id = $this->request->param('id');
        $protocol = ORM::factory('votingprotocol',$id);
        if($protocol->loaded()){
            if($protocol->user_id = Auth::instance()->get_user()->id){
                unlink($protocol->path);
                $protocol->delete();
                $this->response->body(json_encode(array('status'=>'OK')));
            }
            else{
                $this->response->body(json_encode(array('status'=>'FAIL','error'=>__('This protocol belongs to another user'))));
            }
        }
        else{
            $this->response->body(json_encode(array('status'=>'FAIL','error'=>__('No such protocol'))));
        }
    }
    public function action_generate()
    {

    }
 }
