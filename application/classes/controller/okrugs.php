<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Okrugs extends Controller_Admin {

    protected $security_roles = array('admin');
	protected $orm_name = 'okrug';
	
	public function before()
	{
	    parent::before();
	    $this->title('Okrugs');
	}
    /**
    * список мажоритарных округов. (для админа)
    */
   public function action_index()
   {
		$region_id = $this->request->param('id');
		if($region_id){
			$items = ORM::factory('okrug')->where('region_id','=',$region_id)->order_by('name')->find_all();
            $region = ORM::factory('region',$region_id)->name;
			$this->content(View::factory('okrugs/region',array('items'=>$items,'region_id'=>$region_id,'region'=>$region)));
		}
		else{
            $items = ORM::factory($this->orm_name)->order_by('region_id')->order_by('name')->find_all();
            $regions = ORM::factory('region')->select('id','name')->order_by('name')->find_all()->as_array('id','name');
            $this->content(View::factory($this->request->controller().'/index',array('items'=>$items,'regions'=>$regions)));
		}
   }

}
