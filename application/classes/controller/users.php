<?php defined('SYSPATH') or die('No direct script access.');

define ('USERS_ON_PAGE',10);

class Controller_Users extends Controller_Deftemplate {
	protected  $secure_actions     = array(
                        'profile' =>array('login'),
                        'logout' =>array('login'),
						'admin'=>array('login','admin'),
						'adminedit'=>array('login','admin'),
					    'remove' => array('login','admin'),
					    );  
    const CONFIRMATION_STATUS_UNKNOWN = 0;
    const CONFIRMATION_STATUS_UNCONFIRMED = 1;
    const CONFIRMATION_STATUS_PASSPORT = 2;
    const CONFIRMATION_STATUS_PARTYOBSERVERREQUEST = 3;
    const CONFIRMATION_STATUS_PARTYOBSERVERVERIFIED = 4;
	public function before()
	{
	    parent::before();
	    $this->title('Login');
	}

	public function action_login(){
		if(!Auth::instance()->logged_in()){
			$this->inner_login('users/login');
		}
		else{
			$this->redirect('/');
		}
	}
	protected function inner_login($viewname,$viewparams=array()){
		if($this->is_mobile){
		    $this->menu_links = array();
		}
		$username = '';
		$message = '';
		//Attempt login if form was submitted
		if (Arr::get($_POST,'login_form_sent')==1){
			$username = Arr::get($_POST,'username');
			$password = Arr::get($_POST,'password');
			Session::instance()->set('username',$username);
			if (Auth::instance()->login($username,$password,true)){
				$this->session_id = Auth::instance()->get_user()->session_key;
				Cookie::set('voting_session',$this->session_id,3600*24*20);
				$this->request->redirect('/');
				return;
			} else {
				$message = 'Invalid username and/or password.';
			}
		}
		//Display login form
		$view = new View($viewname,$viewparams);
		$view->username = $username;
		$view->password = '';
		$view->message = $message;
		$this->content($view);
	}
	public function action_loginrequired()
	{
	    $this->inner_login('users/login',array('error'=>__('You need to login first')));
	}
	public function action_register($metarole = null)
	{
	    if($this->is_mobile){
    	    $this->menu_links = array();
    	    }
	    if(Auth::instance()->logged_in()!= 0){
		//redirect to the user account
			$this->request->redirect('/');
	    }
	    $this->title(__('Registration'));
	    //Load the view
	    $params['errors'] = '';
	    $params['validation'] = [];

	    //If there is a post and $_POST is not empty
	    if ((Arr::get($_POST,'register_form_sent') == 1 ) )
	    {	
        	if(Captcha::valid(Arr::get($_REQUEST,'captcha')))
        	{
			    
			    //Affects the sanitized vars to the user object
			    
			    $rawpassword = Arr::get($_POST,'password');
			    try
			    {
                    //create the account
		    $this->session_id = $this->generateSessionKey();
		    $user = ORM::factory('user')->create_user(array_merge($_POST,['session_key'=>$this->session_id,'regdate'=>time()]),null);
                    
                    $user->save();
                    $login_role = new Model_Role(array('name' =>'login'));
                    $user->add('roles',$login_role);
		    if($user->confirmed_state != self::CONFIRMATION_STATUS_PARTYOBSERVERREQUEST){
				$user->party_id = 0;
		    }

                    $user->save();
                    $success = true;
                    $error = '';
                    if($user->confirmed_state == self::CONFIRMATION_STATUS_PASSPORT){
                        //upload photo
                        if(isset($_FILES['passport']) && is_uploaded_file($_FILES['passport']['tmp_name'])){
                            mkdir('assets/passports/'.$user->id);
                            $res = move_uploaded_file($_FILES['passport']['tmp_name'],'assets/passports/'.$user->id.'/'.$_FILES['passport']['name']);
                            if(!$res){
                                $error = __('Failed to upload passport photo');
                                $success = false;
                            }
                        }
                        else{
                            //check if confirm_state requires photo uploading
                            $success = false;
                            $error = __('You need to upload passport photo');
                        }
                    }
                    if($success){
                        //sign the user in
                        Auth::instance()->login($user->username, $rawpassword,true);
                        //redirect to the user account
                        $this->request->redirect('/users/successreg');
                    }
                    else{
                        $user->delete();

                        $params['errors'] = $error;
                    }

			    }
			    catch(ORM_Validation_Exception $e){
				$validation = $e->errors('validation',TRUE);
				if(isset($validation['_external']))//dirty hack, hate this
				{
				    $validation = array_merge($validation, $validation['_external']);
				}
    				$params['validation'] = $validation;

			    }
			    catch(Database_Exception $e){
	    			$params['errors'] = $e->getMessage();
			    }
			}
            else{
                $params['errors'] = __('Wrong captcha');
            }
	}
        if(!$this->is_mobile){
            $this->add_script('https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js');
            $this->add_style('div-bootstrap.css');
//            $this->add_style('https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css');
//            $this->add_style('https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css');
        }
        $params['okrugs'] = ORM::factory('okrug')->select('id','name')->where('region_id','=',Arr::get($_POST,'region_id'))->order_by('id')->find_all()->as_array('id','name');
        $params['comissions'] = ORM::factory('comission')->select('id')->where('okrug_id','=',Arr::get($_POST,'okrug_id'))->order_by('id')->find_all()->as_array('id','id');
        $params['regions'] = ORM::factory('region')->where('enabled','=',true)->select('id','name')->order_by('id')->find_all()->as_array('id','name');
        $params['parties'] = ORM::factory('party')->select('id','name')->order_by('id')->find_all()->as_array('id','name');
        $params['captcha'] = Captcha::instance()->render();
        $params['user'] = $_POST;
	    $this->content(View::factory('users/register',$params));
	}
    public function action_addpartyobserver()
    {
        if(!$this->is_mobile){
            if(Auth::instance()->logged_in('party_admin')!= 0){
                $this->add_script('https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js');
        	    $this->add_style('div-bootstrap.css');
                $params = ['validation'=>[],'errors'=>''];

                if(Arr::get($_REQUEST,'register_form_sent') == 1){
		    
                    try
                    {
    	    		    $user = ORM::factory('user')->create_user(array_merge($_POST,['regdate'=>time(),
											'party_id'=>Auth::instance()->get_user()->party_id,
											'confirmed_state'=>Controller_Users::CONFIRMATION_STATUS_PARTYOBSERVERVERIFIED,
											'session_key'=>$this->generateSessionKey()]),null);
                            $login_role = new Model_Role(array('name' =>'login'));
                            $user->add('roles',$login_role);
                            //sign the user in
                            $user->save();
                        $this->request->redirect('/users/partyobservers');
                	return;
                    }
                    catch(ORM_Validation_Exception $e){
                        $validation = $e->errors('validation',TRUE);
                        if(isset($validation['_external'])){//dirty hack, hate this
                            $validation = array_merge($validation, $validation['_external']);
                        }
                        $params['validation'] = $validation;
                    }
                    catch(Database_Exception $e){
                        $params['errors'] = $e->getMessage();
                    }
                }
                
                $user = ORM::factory('user');//empty user for empty fields
		$user->values($_POST);
                
                $params['regions'] = ORM::factory('region')->where('enabled','=',true)->select('id','name')->order_by('id')->find_all()->as_array('id','name');
                $params['okrugs'] = ORM::factory('okrug')->select('id','name')->where('region_id','=',$user->region_id)->order_by('id')->find_all()->as_array('id','name');
                $params['comissions'] = ORM::factory('comission')->select('id')->where('okrug_id','=',$user->okrug_id)->order_by('id')->find_all()->as_array('id','id');
                $params['user'] = $user;
                $this->content(View::factory('users/registerpartyobserver',$params));
            }
            else{
                $this->content(View::factory('error',array('error'=>__('You don\'t have rights to access this page'))));
            }
       	}
        else{
            $this->content(View::factory('error',array('error'=>__('This page is not accessible from mobile devices'))));
        }
    }
    public function action_editpartyobserver()
    {
        if(!$this->is_mobile){
            if(Auth::instance()->logged_in('party_admin')!= 0){
                $this->add_script('https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js');
        	    $this->add_style('div-bootstrap.css');
                $params = ['validation'=>[],'errors'=>''];
                $user_id = $this->request->param('id');
                if($user_id){
                    $user = ORM::factory('user',$user_id);
                    if($user->loaded()){
                        if(Arr::get($_REQUEST,'register_form_sent') == 1){
                            try
                            {
                			    $user = $user->update_user(array_merge($_POST,['party_id'=>Auth::instance()->get_user()->party_id,
        								'confirmed_state'=>Controller_Users::CONFIRMATION_STATUS_PARTYOBSERVERVERIFIED,
        								'session_key'=>$this->generateSessionKey()]),null);
                                $this->request->redirect('/users/partyobservers');
                            	return;
                            }
                            catch(ORM_Validation_Exception $e){
                                $validation = $e->errors('validation',TRUE);
                                if(isset($validation['_external']))//dirty hack, hate this
                                {
                                    $validation = array_merge($validation, $validation['_external']);
                                }
                                $params['validation'] = $validation;var_dump($params['validation']);
                            }
                            catch(Database_Exception $e){
                                $params['errors'] = $e->getMessage();
                            }
                        }
                        $params['regions'] = ORM::factory('region')->where('enabled','=',true)->select('id','name')->order_by('id')->find_all()->as_array('id','name');
                        $params['okrugs'] = ORM::factory('okrug')->select('id','name')->where('region_id','=',$user->region_id)->order_by('id')->find_all()->as_array('id','name');
                        $params['comissions'] = ORM::factory('comission')->select('id')->where('okrug_id','=',$user->okrug_id)->order_by('id')->find_all()->as_array('id','id');
                        $params['user'] = $user;
                        $this->content(View::factory('users/registerpartyobserver',$params));
			return;
                    }
                    else{
                        $this->content(View::factory('error',array('error'=>__('No observer with such id'))));
                        return;
                    }
                }
		else{
		    $this->request->redirect('/users/partyobservers');
		}
            }
            else{
                $this->content(View::factory('error',array('error'=>__('You don\'t have rights to access this page'))));
            }
       	}
        else{
            $this->content(View::factory('error',array('error'=>__('This page is not accessible from mobile devices'))));
        }
    }
    public function action_deletepartyobserver()
    {
        $this->auto_render = false;
        $this->response->headers('Content-Type', 'application/json; charset=utf-8');
        if(Auth::instance()->logged_in('party_admin')!= 0){
            $observer_id = $this->request->param('id');
            $user = ORM::factory('user',$observer_id);
            if($user){
                $user->delete();
                $this->response->body(json_encode(['success'=>true]));
            }
            else{
                $this->response->body(json_encode(['success'=>false,'error'=>__("Observer with this id doesn't exist")]));
            }
        }
        else{
            $this->response->body(json_encode(['success'=>false,'error'=>__('You have no permision to delete this observer')]));
        }
    }
    public function action_confirmpartyobserver()
    {
        $this->auto_render = false;
        $this->response->headers('Content-Type', 'application/json; charset=utf-8');
        if(Auth::instance()->logged_in('party_admin')!= 0){
            $observer_id = $this->request->param('id');
            $user = ORM::factory('user',$observer_id);
            if($user){
                if($user->confirmed_state == self::CONFIRMATION_STATUS_PARTYOBSERVERREQUEST){
                    try{
                        $user->confirmed_state = self::CONFIRMATION_STATUS_PARTYOBSERVERVERIFIED;
                        $user->save();
                        $this->response->body(json_encode(['success'=>true]));
                    }
                    catch(ORM_Validation_Exception $e){
                        $this->response->body(json_encode(['success'=>false, 'error'=>$e->errors()]));
                    }
                }
                else{
                    $this->response->body(json_encode(['success'=>false,'error'=>__("You can't confirm this user as party admin")]));
                }

            }
            else{
                $this->response->body(json_encode(['success'=>false,'error'=>__("Observer with this id doesn't exist")]));
            }
        }
        else{
            $this->response->body(json_encode(['success'=>false,'error'=>__('You have no permision to confirm this observer')]));
        }
    }
    public function action_partyobservers()
    {
        if(!$this->is_mobile){
            if(Auth::instance()->logged_in('party_admin')!= 0){
                $this->add_script('https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js');
                $this->add_style('div-bootstrap.css');
                $params = array();
                $params['observers'] = ORM::factory('user')->select('id','username','fullname','region_id','okrug_id','comission_id')
                    ->where('party_id','=',Auth::instance()->get_user()->party_id)
                    ->where('confirmed_state','in',[self::CONFIRMATION_STATUS_PARTYOBSERVERREQUEST, self::CONFIRMATION_STATUS_PARTYOBSERVERVERIFIED])
                    ->order_by('confirmed_state')
                    ->find_all();
                $this->content(View::factory('users/observers',$params));
                //$this->content(View::factory('error',array('error'=>__('not implemented'))));
            }
            else{
                $this->content(View::factory('error',array('error'=>__('You don\'t have rights to access this page'))));
            }
       	}
        else{
            $this->content(View::factory('error',array('error'=>__('This page is not accessible from mobile devices'))));
        }
    }

	public function action_accessdenied()
	{
		$this->content('Access denied. You must login to get access to this resource',true);
	}
	public function action_successreg(){
		$this->content(View::factory('users/success'));
	}
	public function action_logout(){
		Auth::instance()->logout();
		$this->request->redirect('/');
		//save to session
		//redirect to home
	}
	public function action_index(){
		if(Auth::instance()->logged_in()){
			$this->request->redirect('/');
		}
		else{
			$this->action_login();
		}
	}

	public function action_remove()
	{
        $id = $this->request->param('id');
		$user = ORM::factory('user',$id);
		$user->delete();
		$this->auto_render = false;
        $this->content('OK',true);
	}
	public function action_admin(){
	    $users = ORM::factory('user')->find_all();
	    $this->content(View::factory('users/userlist',array('users'=>$users)));

	}
    public function action_votes()
    {
        $user_id = $this->request->param('id');
        $parties = ORM::factory('partyvote')->join('parties')->on('party_id','=','parties.id')->select('parties.name')->where('user_id','=',$user_id)->find_all();
        $deputies = ORM::factory('deputyvote')->join('deputies')->on('deputy_id','=','deputies.id')->select('deputies.name')->where('user_id','=',$user_id)->find_all();
        $violations = array();//ORM::factory('violations')->where('user_id','=',$user_id)->find_all();
        $this->content(View::factory('users/uservotes',array('parties'=>$parties,'deputies'=>$deputies,'violations'=>$violations)));
    }
	public function action_profile()
	{
		$this->title ('Profile');
		$user = Auth::instance()->get_user();
		$params = $user->as_array();
        $params['error'] = false;
        if(Arr::get($_POST,'profile_sent') == 1)
        {
            if(isset($_POST['new_password']))
            {
                $old_password = Arr::get($_POST,'old_password');
                $new_password = Arr::get($_POST,'new_password');
                $repeat_password = Arr::get($_POST,'repeat_password');
                if(!Auth::instance()->check_password($old_password)){
                    $params['error'] = ___('Old password is wrong');
                }
                elseif($new_password != $repeat_password){
                    $params['error'] = ___('Passwords are not same');
                }
                $user->values($_POST);
                $user->password = $new_password;
            }
            else
            {
                unset($_POST['password']);
                $user->values($_POST);
            }
            if(!$params['error'])
            {
                try{
                    $user->save();
                }
                catch(ORM_Validation_Exception $e)
                {
                    $params['error'] = $e->getMessage();
                }
            }
        }
		$this->content(View::factory('users/profile'),$params);
	}
    public function action_adminedit($id)
    {
        $user = ORM::factory('user',$id);
        $error = false;
        if(Arr::get($_POST,'adminedit_sent') == 1)
        {
            $user->values($_POST);
            try{
                $user->save();
            }
            catch(ORM_Validation_Exception $e)
            {
                $error = $e->getMessage();
            }
        }
        $userdata = $user->as_array();
        $userdata['error'] = $error;
        $this->content(View::factory('users/adminedit'),$userdata);
    }
    /**
    * returns html formatted data ready to insert into <select> to populate is with okrugs
    */
    public function action_okrugslist()
    {
	$this->auto_render = false;
	$this->response->headers('Content-Type', 'application/json; charset=utf-8');
	$region_id = $this->request->param('id');
	$okrugs = ORM::factory('okrug')->select('id','name')->where('region_id','=',$region_id)->order_by('name')->find_all()->as_array('id','name');
	$this->response->body(json_encode($okrugs));
    }

    /**
    * returns  data ready to insert into <select> to populate is with comissions
    */
    public function action_comissionlist()
    {
	$this->auto_render = false;
	$this->response->headers('Content-Type', 'application/json; charset=utf-8');
	$okrug_id = $this->request->param('id');
	$comissions = ORM::factory('comission')->select('id')->where('okrug_id','=',$okrug_id)->order_by('name')->find_all()->as_array(null,'id');
	$this->response->body(json_encode($comissions));
    }
/*
private function socketmail($to, $from, $subject, $message) { 
    $connect = fsockopen ('ssl://smtp.gmail.com', 465, $errno, $errstr, 30); 
    var_dump($connect);
    fputs($connect, "EHLO gmail.com\r\n"); 
    var_dump(fread($connect,1000));
    fputs($connect, "AUTH LOGIN\r\n"); 
    var_dump(fread($connect,1000));
    fputs($connect, base64_encode("admin@t4log.com")."\r\n"); 
    var_dump(fread($connect,1000));
    fputs($connect, base64_encode("Cdbltntkm#20")."\r\n"); 
    var_dump(fread($connect,1000));
    fputs($connect, "MAIL FROM: $from\n"); 
    var_dump(fread($connect,1000));
    fputs($connect, "RCPT TO: $to\n"); 
    var_dump(fread($connect,1000));
    fputs($connect, "DATA\r\n"); 
    fputs($connect, "Content-Type: text/plain; charset=iso-8859-1\n"); 
    fputs($connect, "To: $to\n"); 
    fputs($connect, "Subject: $subject\n"); 
    fputs($connect, "\n\n"); 
    fputs($connect, stripslashes($message)." \r\n"); 
    fputs($connect, ".\r\n"); 
    fputs($connect, "RSET\r\n"); 
    var_dump(fread($connect,1000));

    fclose($connect);
    return '';
} 
    public function action_email()
    {
	//mail('demo_s@ua.fm','test','test body');
	//mail('demosglok@gmail.com','test','test body');
	$res = $this->socketmail('demo_s@ua.fm','admin@t4log.com','test','test body');
	//$res2 = $this->socketmail('demosglok@gmail.com','demosglok@gmail.com','test','test body');
	//$res = $res1 . ' || '.$res2;
	$this->content('test email.'.$res,true);
    }
*/
 } 
