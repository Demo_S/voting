<?php defined('SYSPATH') or die('No direct script access.');

class Model_Votingprotocol extends ORM {
	protected $_table_name = 'votes_protocols';
    protected $_filters = array(TRUE => array('trim' => NULL));
    public static $TYPE_PARTY_PROTOCOL = 1;
    public static $TYPE_DEPUTY_PROTOCOL = 2;
    public function rules()
    {
	return array(
	    'user_id'=>array(
		array('numeric'),
	    ),		
	    'votes_id'=>array(
		array('numeric'),
	    ),
	    'path'=>array(
            array('max_length',array(':value',250))
	    ),
	    'ts'=>array(
		array('numeric'),
	    ),
        'oik_id'=>array(
   		array('numeric'),
   	    ),
        'ik_id'=>array(
   		array('numeric'),
   	    ),
        'type'=>array(
            array('numeric')
        ),
		'vkontakte_path'=>array(
			array('max_length',array(':value',200))
		),
		'vkontakte'=>array(
			array('numeric'),
		),

	);
    }

} // END Model
