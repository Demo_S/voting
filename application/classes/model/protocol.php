<?php defined('SYSPATH') or die('No direct script access.');

class Model_Protocol extends ORM {

    protected $_filters = array(TRUE => array('trim' => NULL));
    public function rules()
    {
	return array(
	    'comment'=>array( 
		array('max_length',array(':value',5000)) 
	    ),
	    'user_id'=>array( 
		array('numeric') 
	    ),
	    'ts'=>array( 
		array('numeric') 
	    ),

	);
    }

} // END Model
