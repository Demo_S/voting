<?php defined('SYSPATH') or die('No direct script access.');

class Model_Violation extends ORM {

    protected $_filters = array(TRUE => array('trim' => NULL));
    public function rules()
    {
	return array(
	    'user_id'=>array(
		array('numeric'),
	    ),
	    'type'=>array(
		array('numeric'),
	    ),
        'description'=>array(
            array('max_length',array(':value',5000))
        ),
        'ts'=>array(
   		array('numeric'),
   	    ),
	    'oik_id'=>array(
		array('numeric'),
	    ),
	    'ik_id'=>array(
		array('numeric'),
	    ),

	);
    }

} // END Model
