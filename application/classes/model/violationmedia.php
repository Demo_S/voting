<?php defined('SYSPATH') or die('No direct script access.');

class Model_Violationmedia extends ORM {
	protected $_table_name = 'violations_protocols';
    protected $_filters = array(TRUE => array('trim' => NULL));
    public function rules()
    {
        return array(
       	    'user_id'=>array(
       		array('numeric'),
       	    ),
       	    'violation_id'=>array(
       		array('numeric'),
       	    ),
       	    'path'=>array(
                   array('max_length',array(':value',250))
       	    ),
       	    'ts'=>array(
       		array('numeric'),
       	    ),
           'oik_id'=>array(
            array('numeric'),
            ),
           'ik_id'=>array(
            array('numeric'),
            ),
			'vkontakte_path'=>array(
				array('max_length',array(':value',200))
			),
			'vkontakte'=>array(
				array('numeric'),
			),

       	);
    }

} // END Model
