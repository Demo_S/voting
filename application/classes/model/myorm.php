<?php
defined('SYSPATH') or die('No direct script access.');

class Model_Myorm extends ORM {
	/**
	 * Removes all relationships between this model and another.
	 *
	 * @param   string   alias of the has_many "through" relationship
	 * @return  ORM
	 */
	public function remove_all($alias)
	{
		DB::delete($this->_has_many[$alias]['through'])
			->where($this->_has_many[$alias]['foreign_key'], '=', $this->pk())
			->execute($this->_db);

		return $this;
	}
}