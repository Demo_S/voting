<?php defined('SYSPATH') or die('No direct script access.');

class Model_Party extends ORM {

    protected $_filters = array(TRUE => array('trim' => NULL));
    public function rules()
    {
	return array(
	    'name'=>array( 
		array('max_length',array(':value',250)) 
	    ),
	    'number_in_votelist'=>array(
		array('numeric'),
		
	    ),

	);
    }

} // END Model
