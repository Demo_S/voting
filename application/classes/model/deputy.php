<?php defined('SYSPATH') or die('No direct script access.');

class Model_Deputy extends ORM {

    protected $_filters = array(TRUE => array('trim' => NULL));
    public function rules()
    {
	return array(
	    'name'=>array( 
		array('max_length',array(':value',250)) 
	    ),
	    'okrug_id'=>array(
		array('numeric'),
	    ),
	    'party_id'=>array(
		array('numeric'),
	    ),

	);
    }

} // END Model
