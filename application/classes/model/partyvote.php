<?php defined('SYSPATH') or die('No direct script access.');

class Model_Partyvote extends ORM {
	protected $_table_name = 'parties_votes';
    protected $_filters = array(TRUE => array('trim' => NULL));
    public function rules()
    {
	return array(
	    'user_id'=>array(
		array('numeric'),
	    ),		
	    'party_id'=>array(
		array('numeric'),
	    ),
        'ik_id'=>array(
   		array('numeric'),
   	    ),
	    'votes'=>array(
		array('numeric'),
	    ),
	    'ts'=>array(
		array('numeric'),
	    ),

	);
    }

} // END Model
