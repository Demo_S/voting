<?php defined('SYSPATH') or die('No direct script access.');

class Model_Okrug extends ORM {

    protected $_filters = array(TRUE => array('trim' => NULL));
    public function rules()
    {
	return array(
	    'name'=>array( 
		array('max_length',array(':value',250)) 
	    ),
	    'region_id'=>array( 
		array('numeric') 
	    ),
        'number'=>array(
            array('numeric')
        ),
        'details'=>array(
            array('max_length',array(':value',250))
        )
	);
    }

} // END Model
