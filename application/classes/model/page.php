<?php defined('SYSPATH') or die('No direct script access.');

class Model_Page extends ORM {


    public function rules()
    {
	return array(
	    'innername'=>array( 
		array('max_length',array(':value',250)) 
	    ),
	    'title'=>array( 
		array('max_length',array(':value',250)) 
	    ),
	    'content'=>array( 
		array('max_length',array(':value',5000)) 
	    ),

	);
    }

} // END Model
