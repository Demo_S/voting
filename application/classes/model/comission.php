<?php defined('SYSPATH') or die('No direct script access.');

class Model_Comission extends ORM {

    protected $_filters = array(TRUE => array('trim' => NULL));
    public function rules()
    {
	return array(
	    'name'=>array( 
		array('max_length',array(':value',250)) 
	    ),
	    'okrug_id'=>array(
			array('numeric') 
	    ),
        'bounds'=>array(
   		array('max_length',array(':value',5000))
   	    ),
        'address'=>array(
   		array('max_length',array(':value',250))
   	    ),
        'numpeople'=>array(
   			array('numeric')
   	    ),
        'numvotes'=>array(
   			array('numeric')
   	    ),

	);
    }

} // END Model
