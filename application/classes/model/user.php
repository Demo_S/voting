<?php
defined('SYSPATH') or die('No direct script access.');

class Model_User extends Model_Auth_User {
 	
    protected $_belongs_to = ['region'=>['model'=>'region'],
                            'okrug'=>['model'=>'okrug'],
                            'comission'=>['model'=>'comission'],
			    'party'=>['model'=>'party']];
	/* Validation rules
	 * Rules for the user model. Because the password is _always_ a hash
	 * when it's set,you need to run an additional not_empty rule in your controller
	 * to make sure you didn't hash an empty string. The password rules
	 * should be enforced outside the model or with a model helper method.
	 *
	 * @return array Rules
	 */
	public function rules()
	{
		return array(
			'username' => array(
				array('not_empty'),
				array('min_length', array(':value', 4)),
				array('max_length', array(':value', 32)),
				array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
            			array(array($this, 'unique'), array('username', ':value')),
			),
			'password' => array(
				array('not_empty'),
				
			),
			
			'email' => array(
				array('not_empty'),
				array('min_length', array(':value', 4)),
				array('max_length', array(':value', 127)),
				array('email'),
                array(array($this, 'unique'), array('email', ':value')),
			),
            'okrug_id' => array (
                array('numeric'),
		array('not_empty')
            ),
            'comission_id' => array (
                array('numeric'),
		array('not_empty')
            ),
            'party_id' => array (
                array('numeric'),
            ),
            'region_id'=>array(
        	array('numeric'),
		array('not_empty')
            ),
            'fullname'=>array(
                array('max_length',array(':value',255))
            ),
            'show_fio' => array(
                array('numeric'),
            ),
            'authorized' => array(
                array('numeric'),
            ),
	    'phone' => [
		['phone']
	    ],
            'regdate' => array(
                array('numeric'),
            ),
            'confirmed_state' => array(
                array('numeric'),
            ),
			'session_key'=>array(
				array('numeric'),
				array('max_length',array(':value',24)),
			),
		);
	}
    public function labels()
    {
        return [
            'username' => __('Username'),
            'email' => __('Email'),
            'password' => __('Password'),
	    'password_confirm' => __('Repeat password'),
            'fullname' => __('Full name'),
            'party_id' => __('Party'),
            'region_id' => __('Region'),
            'okrug_id' => __('Okrug'),
            'comission_id' => __('Comission'),
            'confirmed_state' => __('Уровень подтверждения идентичности')
        ];
    }
	public function replace_roles($roles)
	{
		DB::query(Database::DELETE,"delete from roles_users where user_id=".$this->pk())->execute();
		if(count($roles))
		{
			$pairs = array();
			foreach($roles as $roleid)
				$pairs[] = '('.(int)$this->id.','.(int)$roleid.')';
			try 
			{
				DB::query(Database::INSERT,"replace into roles_users values ".implode(',',$pairs))->execute();
				//DB::insert('orderscomments_roles', array('comment_id','role_id'))->values($pairs)->execute();
			return true;
			}
			catch( Database_Exception $e )
			{
	   			echo $e->getMessage();
			}
		}													 

	}


}